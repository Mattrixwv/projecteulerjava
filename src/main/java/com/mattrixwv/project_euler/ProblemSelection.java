//ProjectEuler/ProjectEulerJava/src/man/java/mattrixwv/ProjectEuler/ProblemSelection.java
//Matthew Ellison
// Created: 07-08-20
//Modified: 07-09-20
//This class holds all of the functions needed to handle a problem
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler;


import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.mattrixwv.exceptions.InvalidResult;

import com.mattrixwv.project_euler.problems.*;


public class ProblemSelection{
	private static final Scanner input = new Scanner(System.in);
	//Holds the valid problem numbers
	protected static final List<Integer> PROBLEM_NUMBERS = Arrays.asList( 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
																			 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
																			 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
																			 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
																			 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
																			 67);

	private ProblemSelection(){
	}

	//Returns the problem corresponding to the given problem number
	public static Problem getProblem(Integer problemNumber){
		Problem problem = null;
		switch(problemNumber){
			case 1  : problem = new Problem1();  break;
			case 2  : problem = new Problem2();  break;
			case 3  : problem = new Problem3();  break;
			case 4  : problem = new Problem4();  break;
			case 5  : problem = new Problem5();  break;
			case 6  : problem = new Problem6();  break;
			case 7  : problem = new Problem7();  break;
			case 8  : problem = new Problem8();  break;
			case 9  : problem = new Problem9();  break;
			case 10 : problem = new Problem10(); break;
			case 11 : problem = new Problem11(); break;
			case 12 : problem = new Problem12(); break;
			case 13 : problem = new Problem13(); break;
			case 14 : problem = new Problem14(); break;
			case 15 : problem = new Problem15(); break;
			case 16 : problem = new Problem16(); break;
			case 17 : problem = new Problem17(); break;
			case 18 : problem = new Problem18(); break;
			case 19 : problem = new Problem19(); break;
			case 20 : problem = new Problem20(); break;
			case 21 : problem = new Problem21(); break;
			case 22 : problem = new Problem22(); break;
			case 23 : problem = new Problem23(); break;
			case 24 : problem = new Problem24(); break;
			case 25 : problem = new Problem25(); break;
			case 26 : problem = new Problem26(); break;
			case 27 : problem = new Problem27(); break;
			case 28 : problem = new Problem28(); break;
			case 29 : problem = new Problem29(); break;
			case 30 : problem = new Problem30(); break;
			case 31 : problem = new Problem31(); break;
			case 32 : problem = new Problem32(); break;
			case 33 : problem = new Problem33(); break;
			case 34 : problem = new Problem34(); break;
			case 35 : problem = new Problem35(); break;
			case 36 : problem = new Problem36(); break;
			case 37 : problem = new Problem37(); break;
			case 38 : problem = new Problem38(); break;
			case 39 : problem = new Problem39(); break;
			case 40 : problem = new Problem40(); break;
			case 41 : problem = new Problem41(); break;
			case 42 : problem = new Problem42(); break;
			case 43 : problem = new Problem43(); break;
			case 44 : problem = new Problem44(); break;
			case 45 : problem = new Problem45(); break;
			case 46 : problem = new Problem46(); break;
			case 47 : problem = new Problem47(); break;
			case 48 : problem = new Problem48(); break;
			case 49 : problem = new Problem49(); break;
			case 50 : problem = new Problem50(); break;
			case 67 : problem = new Problem67(); break;
			default: throw new InvalidParameterException();
		}
		return problem;
	}
	//Print the description of a problem
	public static void printDescription(Integer problemNumber){
		//Get the problem
		Problem problem = getProblem(problemNumber);
		//Print the problem's description
		System.out.println(problem.getDescription());
	}
	//Solve a problem
	public static void solveProblem(Integer problemNumber) throws InvalidResult{
		//Get the problem
		Problem problem = getProblem(problemNumber);
		//Print the problem description
		System.out.println(problem.getDescription());
		//Solve the problem
		problem.solve();
		//Print the results
		System.out.println(problem.getResult() + "\nIt took " + problem.getTime() + " to solve this problem.\n\n");
	}
	//Get a valid problem number from a user
	public static Integer getProblemNumber(){
		Integer problemNumber = 0;
		System.out.print("Enter a problem number: ");
		problemNumber = input.nextInt();
		while(!PROBLEM_NUMBERS.contains(problemNumber)){
			System.out.print("That is an invalid problem number!\nEnter a problem number: ");
			problemNumber = input.nextInt();
		}
		return problemNumber;
	}
	//List all valid problem numbers
	public static void listProblems(){
		System.out.print(PROBLEM_NUMBERS.get(1));
		for(Integer problemNumber = 2;problemNumber < PROBLEM_NUMBERS.size();++problemNumber){
			System.out.print(", " + PROBLEM_NUMBERS.get(problemNumber).toString());
		}
		System.out.println();
	}
}
