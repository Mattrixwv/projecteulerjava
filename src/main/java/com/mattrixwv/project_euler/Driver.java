//ProjectEuler/ProjectEulerJava/src/man/java/mattrixwv/ProjectEuler/Driver.java
//Matthew Ellison
// Created: 06-07-20
//Modified: 07-09-20
//This is the driver function for the Java version of the ProjectEuler project
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler;


import java.util.Scanner;

import com.mattrixwv.exceptions.InvalidResult;


public class Driver{
	//An enum to hold the possible menu selections
	private enum SELECTIONS{SOLVE, DESCRIPTION, LIST, BENCHMARK, EXIT, SIZE}
	private static final Scanner input = new Scanner(System.in);

	//Drives the program
	public static void main(String[] args) throws InvalidResult{
		SELECTIONS selection;	//Holds the menu selection of the user
		do{
			//Print the menu and prompt the user to select an action
			printMenu();
			selection = getMenuSelection();

			switch(selection){
				case SOLVE : solveMenu(); break;
				case DESCRIPTION : descriptionMenu(); break;
				case LIST : ProblemSelection.listProblems(); break;
				case BENCHMARK : Benchmark.benchmarkMenu(); break;
				case EXIT : break;
				case SIZE : 
				default : printErrorMessage();
			}
		}while(!selection.equals(SELECTIONS.EXIT));
	}
	//Print the menu
	private static void printMenu(){
		System.out.println("1. Solve a problem");
		System.out.println("2. Print a problem description");
		System.out.println("3. List valid problem numbers");
		System.out.println("4. Benchmark");
		System.out.println("5. Exit");
		System.out.println();
	}
	//Get a menu selection from the user
	private static SELECTIONS getMenuSelection(){
		Integer selection = input.nextInt();
		while(!isValidMenu(selection)){
			System.out.println("That is an invalid option!\nPress Enter to continue");
			printMenu();
			selection = input.nextInt();
		}
		return getSelection(selection);
	}
	//Make sure the value passed in is a valid menu option
	private static boolean isValidMenu(Integer selection){
		//Ordinal + 1 because enum starts at 0
		return ((selection > 0) && (selection < (SELECTIONS.SIZE.ordinal() + 1)));
	}
	//Turns an integer passed to it into a SELECTION enum
	private static SELECTIONS getSelection(Integer selection){
		SELECTIONS sel = null;

		switch(selection){
			case 1 : sel = SELECTIONS.SOLVE; break;
			case 2 : sel =  SELECTIONS.DESCRIPTION; break;
			case 3 : sel = SELECTIONS.LIST; break;
			case 4 : sel = SELECTIONS.BENCHMARK; break;
			case 5 : sel = SELECTIONS.EXIT; break;
			default : sel = SELECTIONS.SIZE;
		}
		return sel;
	}
	//Print an error message
	private static void printErrorMessage(){
		System.out.println("That is an invalid selection!");
	}
	//Handle what happens when a user wants to solve a problem
	private static void solveMenu() throws InvalidResult{
		Integer problemNumber = ProblemSelection.getProblemNumber();
		//This selection solves all problems in order
		if(problemNumber.equals(0)){
			//Solve to every valid problem number, skipping over 0
			for(Integer problemLocation = 1;problemLocation < ProblemSelection.PROBLEM_NUMBERS.size();++problemLocation){
				//Solve the problems
				System.out.print(ProblemSelection.PROBLEM_NUMBERS.get(problemLocation).toString() + ". ");
				ProblemSelection.solveProblem(ProblemSelection.PROBLEM_NUMBERS.get(problemLocation));
			}
		}
		//This is if a single problem number was chosen
		else{
			//Solve the problem
			ProblemSelection.solveProblem(problemNumber);
		}
	}
	//Handle what happens when a user wants to see the description of a problem
	private static void descriptionMenu(){
		//Give some extra space to print the description
		System.out.println("\n");

		//Get the problem number
		Integer problemNumber = ProblemSelection.getProblemNumber();

		//If the problem number is 0 print out all the descriptions
		if(problemNumber.equals(0)){
			//Print description for every valid problem number, skipping over 0
			for(Integer problemLocation = 1;problemLocation < ProblemSelection.PROBLEM_NUMBERS.size();++problemLocation){
				//Print the problem's description
				System.out.print(ProblemSelection.PROBLEM_NUMBERS.get(problemLocation) + ". ");
				ProblemSelection.printDescription(ProblemSelection.PROBLEM_NUMBERS.get(problemLocation));
				System.out.println();
			}
		}
		//Otherwise print out a single problem's description
		else{
			ProblemSelection.printDescription(problemNumber);
		}
	}
}
