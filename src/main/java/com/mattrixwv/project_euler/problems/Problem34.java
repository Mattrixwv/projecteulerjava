//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem34.java
//Matthew Ellison
// Created: 02-05-21
//Modified: 06-27-23
//Find the sum of all numbers which are equal to the sum of the factorial of their digits
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.NumberAlgorithms;


public class Problem34 extends Problem{
	//Variables
	//Static variables
	private static final int MAX_NUM = 1499999;	//The largest num that can be the sum of its own digits
	//Instance variables
	protected List<Integer> factorials;	//Holds the pre-computed factorials of the numbers 0-9
	protected int sum;	//Holds the sum of all numbers equal to the sum of their digit's factorials

	//Functions
	//Constructor
	public Problem34(){
		super("Find the sum of all numbers which are equal to the sum of the factorial of their digits");
		sum = 0;
		factorials = new ArrayList<>();
		for(int cnt = 0;cnt <= 9;++cnt){
			factorials.add(0);
		}
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Pre-compute the possible factorials from 0! to 9!
		for(int cnt = 0;cnt <= 9;++cnt){
			factorials.set(cnt, NumberAlgorithms.factorial(cnt));
		}
		//Run through all possible numbers from 3-MAX_NUM and see if they equal the sum of their digit's factorials
		for(int cnt = 3;cnt < MAX_NUM;++cnt){
			//Split the number into its digits and add each one to the sum
			String numString = "" + cnt;
			int currentSum = 0;
			for(int charCnt = 0;charCnt < numString.length();++charCnt){
				Character digit = numString.charAt(charCnt);
				int tempNum = Integer.parseInt(digit.toString());
				currentSum += factorials.get(tempNum);
			}
			//If the number is equal to the sum add the sum to the running sum
			if(currentSum == cnt){
				sum += currentSum;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		sum = 0;
		factorials.clear();
		for(int cnt = 0;cnt <= 9;++cnt){
			factorials.add(0);
		}
	}

	//Gets
	//Returns a string witht he solution to the problem
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all numbers that are the sum of their digit's factorials is %d", sum);
	}
	//Returns the list of factorials from 0-9
	public List<Integer> getFactorials(){
		solvedCheck("list of factorials");
		return factorials;
	}
	//Returns the sum of all numbers equal to the sum of their digit's factorials
	public int getSum(){
		solvedCheck("sum");
		return sum;
	}
}


/* Results:
The sum of all numbers that are the sum of their digit's factorials is 40730
It took an average of 78.889 milliseconds to run this problem through 100 iterations
*/
