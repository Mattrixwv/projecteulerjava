//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem21.java
//Matthew Ellison
// Created: 03-18-19
//Modified: 06-27-23
//Evaluate the sum of all the amicable numbers under 10000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.ArrayAlgorithms;
import com.mattrixwv.NumberAlgorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Problem21 extends Problem{
	//Variables
	//Static variables
	private static final int LIMIT = 10000;	//The number that is > the largest number to be checked
	//Instance variables
	protected ArrayList<Integer> divisorSum;	//Holds the sum of the divisors of the subscript number
	protected ArrayList<Integer> amicable;	//Holds all amicable numbers

	//Functions
	//Constructor
	public Problem21(){
		super(String.format("Evaluate the sum of all the amicable numbers under %d", LIMIT));
		divisorSum = new ArrayList<>();
		amicable = new ArrayList<>();
		reserveArray();
	}
	//Operational functions
	//Reserve the size of the array to speed up insertion
	private void reserveArray(){
		divisorSum.ensureCapacity(LIMIT);	//Reserving it now makes it faster later
		//Make sure the arraylist is filled with 0's
		while(divisorSum.size() < LIMIT){
			divisorSum.add(0);
		}
	}
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Generate the divisors of all numbers < 10000, get their sum, and add it to the list
		for(int cnt = 1;cnt < LIMIT;++cnt){
			List<Integer> divisors = NumberAlgorithms.getDivisors(cnt);	//Get all the divisors of a number
			if(divisors.size() > 1){
				divisors.remove(divisors.get(divisors.size() - 1));	//Remove the last entry because it will be the number itself
			}
			divisorSum.set(cnt, ArrayAlgorithms.getSum(divisors));	//Add the sum of the divisors of the vector
		}
		//Check every sum of divisors in the list for a matching sum
		for(int cnt = 1;cnt < divisorSum.size();++cnt){
			int sum = divisorSum.get(cnt);
			//If the sum is greater than the number of divisors then it is impossible to be amicable. Skip the number and continue
			if(sum >= divisorSum.size()){
				continue;
			}
			//We know that divisorSum.at(cnt) == sum, so if divisorSum.at(sum) == cnt we found an amicable number
			if(divisorSum.get(sum).compareTo(cnt) == 0){
				//A number can't be amicable with itself
				if(sum == cnt){
					continue;
				}
				//Add it to the arraylist of amicable numbers
				amicable.add(cnt);
			}
		}

		//Sort the arraylist for neatness
		Collections.sort(amicable);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		divisorSum.clear();
		amicable.clear();
		reserveArray();
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		StringBuilder result = new StringBuilder(String.format("All amicable numbers less than %d are%n", LIMIT));
		for(int cnt = 0;cnt < amicable.size();++cnt){
			result.append(String.format("%d%n", amicable.get(cnt)));
		}
		result.append(String.format("The sum of all of these amicable numbers is %d", ArrayAlgorithms.getSum(amicable)));

		return result.toString();
	}
	//Returns a vector with all of the amicable numbers calculated
	public List<Integer> getAmicable(){
		solvedCheck("amicable numbers");
		return amicable;
	}
	//Returns the sum of all of the amicable numbers
	public int getSum(){
		solvedCheck("sum of the amicable numbers");
		return ArrayAlgorithms.getSum(amicable);
	}
}


/* Results:
All amicable numbers less than 10000 are
220
284
1184
1210
2620
2924
5020
5564
6232
6368
The sum of all of these amicable numbers is 31626
It took an average of 9.848 milliseconds to run this problem through 100 iterations
*/
