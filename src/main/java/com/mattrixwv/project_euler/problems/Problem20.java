//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem20.java
//Matthew Ellison
// Created: 03-14-19
//Modified: 06-27-23
//What is the sum of the digits of 100!?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.math.BigInteger;


public class Problem20 extends Problem{
	//Variables
	//Static variables
	private static final int TOP_NUM = 100;	//The largest number that will be multiplied
	//Instance variables
	protected BigInteger num;	//Holds the number 100!
	protected long sum;	//The sum of the digits of num

	//Functions
	//Constructor
	public Problem20(){
		super(String.format("What is the sum of the digits of %d!?", TOP_NUM));
		num = BigInteger.ONE;
		sum = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Run through every number from 1 to 100 and multiply it by the current num to generate 100!
		for(int cnt = TOP_NUM;cnt > 1;--cnt){
			num = num.multiply(BigInteger.valueOf(cnt));
		}

		//Get a string of the number because it is easier to pull apart the individual characters
		String numString = num.toString();
		//Run through every character in the string, convert it back to an integer and add it to the running sum
		for(int cnt = 0;cnt < numString.length();++cnt){
			Character digit = numString.charAt(cnt);
			sum += Integer.valueOf(digit.toString());
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		num = BigInteger.ONE;
		sum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("%d! = %s%nThe sum of the digits is: %d", TOP_NUM, num.toString(), sum);
	}
	//Returns the number 100!
	public BigInteger getNumber(){
		solvedCheck("number");
		return num;
	}
	//Returns the number 100! in a string
	public String getNumberString(){
		solvedCheck("number as a string");
		return num.toString(10);
	}
	//Returns the sum of the digits of 100!
	public long getSum(){
		//If the problem hasn't been solved throw an exception
		solvedCheck("sum of the digits");
		return sum;
	}
}


/* Restuls:
100! = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
The sum of the digits is: 648
It took an average of 91.162 microseconds to run this problem through 100 iterations
*/
