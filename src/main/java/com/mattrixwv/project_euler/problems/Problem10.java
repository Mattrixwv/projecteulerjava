//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem10.java
//Matthew Ellison
// Created: 03-03-19
//Modified: 06-27-23
//Find the sum of all the primes below two million
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.ArrayAlgorithms;
import com.mattrixwv.NumberAlgorithms;


public class Problem10 extends Problem{
	//Variables
	//Static variables
	private static final long GOAL_NUMBER = 2000000L - 1L;	//The largest number to check for primes
	//Instance variables
	protected long sum;	//The sum of all of the prime numbers

	//Functions
	//Constructor
	public Problem10(){
		super(String.format("Find the sum of all the primes below %d.", GOAL_NUMBER + 1));
		sum = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get the sum of all prime numbers < GOAL_NUMBER
		sum = ArrayAlgorithms.getLongSum(NumberAlgorithms.getPrimes(GOAL_NUMBER));	//Subtract 1 from the number so that it is < the number


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		sum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all the primes < %d is %d", GOAL_NUMBER + 1, sum);
	}
	//Returns the sum that was requested
	public long getSum(){
		solvedCheck("sum");
		return sum;
	}
}


/* Results:
The sum of all the primes < 2000000 is 142913828922
It took an average of 186.751 milliseconds to run this problem through 100 iterations
*/
