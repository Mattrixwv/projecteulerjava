//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem35.java
//Matthew Ellison
// Created: 06-05-21
//Modified: 06-27-23
//How many circular primes are there below one million?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.NumberAlgorithms;


public class Problem35 extends Problem{
	//Variables
	//Static variables
	protected static int maxNum = 999999;	//The largest number that we are checking for primes
	//Instance variables
	protected List<Integer> primes;	//The primes below MAX_NUM
	protected List<Integer> circularPrimes;	//The circular primes below MAX_NUM
	//Functions
	//Returns a list of all rotations of a string passed to it
	private ArrayList<String> getRotations(String str){
		ArrayList<String> rotations = new ArrayList<>();
		rotations.add(str);
		for(int cnt = 1;cnt < str.length();++cnt){
			str = str.substring(1) + str.substring(0, 1);
			rotations.add(str);
		}
		return rotations;
	}
	//Constructor
	public Problem35(){
		super(String.format("How many circular primes are there below %d?", maxNum + 1));
		primes = new ArrayList<>();
		circularPrimes = new ArrayList<>();
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get all primes under 1,000,000
		primes = NumberAlgorithms.getPrimes(maxNum);
		//Go through all primes, get all their rotations, and check if those numbers are also primes
		for(int prime : primes){
			boolean allRotationsPrime = true;
			//Get all of the rotations of the prime and see if they are also prime
			ArrayList<String> rotations = getRotations(Integer.toString(prime));
			for(String rotation : rotations){
				int p = Integer.parseInt(rotation);
				if(!primes.contains(p)){
					allRotationsPrime = false;
					break;
				}
			}
			//If all rotations are prime add it to the list of circular primes
			if(allRotationsPrime){
				circularPrimes.add(prime);
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		primes.clear();
		circularPrimes.clear();
	}
	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("result");
		return String.format("The number of all circular prime numbers under %d is %d", maxNum, circularPrimes.size());
	}
	//Returns the ArrayList of primes < MAX_NUM
	public List<Integer> getPrimes(){
		solvedCheck("list of primes");
		return primes;
	}
	//Returns the ArrayList of circular primes < MAX_NUM
	public List<Integer> getCircularPrimes(){
		solvedCheck("list of circular primes");
		return circularPrimes;
	}
	//Returns the number of circular primes
	public int getNumCircularPrimes(){
		solvedCheck("number of circular primes");
		return circularPrimes.size();
	}
}


/* Results:
The number of all circular prime numbers under 999999 is 55
It took an average of 5.255 seconds to run this problem through 100 iterations
*/
