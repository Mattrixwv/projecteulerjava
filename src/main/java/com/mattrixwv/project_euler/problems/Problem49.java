//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem49.java
//Mattrixwv
// Created: 08-23-22
//Modified: 06-30-23
//What is the 12-digit number formed by concatenating the three terms in the sequence?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mattrixwv.NumberAlgorithms;
import com.mattrixwv.StringAlgorithms;


public class Problem49 extends Problem{
	//Variables
	//Static variables
	private static final int MIN_NUMBER = 1000;
	private static final int MAX_NUMBER = 9999;
	//Instance variables
	protected String concatenationOfNumbers;

	//Functions
	//Constructor
	public Problem49(){
		super("What is the 12-digit number formed by concatenating the three terms in the sequence?");
		concatenationOfNumbers = "";
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get all of the prime numbers <= MAX_NUMBER
		List<Integer> primes = NumberAlgorithms.getPrimes(MAX_NUMBER);
		//Loop through every prime from min to max and check if its permutations match the sequence
		for(Integer prime : primes){
			//Skip all primes < MIN
			if(prime < MIN_NUMBER){
				continue;
			}
			//Get all permutations of the number
			List<String> primeStrsRaw = StringAlgorithms.getPermutations(prime.toString());
			HashSet<String> primeStrs = new HashSet<>();
			for(String str : primeStrsRaw){
				primeStrs.add(str);
			}
			//Skip the provided example
			if(primeStrs.contains("1487")){
				continue;
			}
			//Remove any even numbers and numbers smaller than the current number
			List<Integer> remaining = removeEven(primeStrs, prime);
			//Check if there are at least 3 elements remaining
			List<Integer> matches = checkRemaining(remaining);
			Collections.sort(matches);
			//If there are at least 3 matches see if there are any that are equidistant
			if(matches.size() >= 3){
				checkEquidistant(matches);
			}
			if(!concatenationOfNumbers.isBlank()){
				break;
			}
		}


		//Stop the timer
		timer.stop();

		//Set a flag to show the problem is solved
		solved = true;
	}
	//Remove any even and small numbers
	public List<Integer> removeEven(Set<String> primeStrs, Integer prime){
		ArrayList<Integer> remaining = new ArrayList<>();
		for(String primeStr : primeStrs){
			Integer num = Integer.valueOf(primeStr);
			if(((num % 2) != 0) && (num >= prime)){
				remaining.add(num);
			}
		}
		return remaining;
	}
	//Check that there are at least 3 prime numbers remaining
	private List<Integer> checkRemaining(List<Integer> remaining){
		ArrayList<Integer> matches = new ArrayList<>();
		if(remaining.size() >= 3){
			//Check if 3 elements are prime
			for(Integer num : remaining){
				if(NumberAlgorithms.isPrime(num)){
					matches.add(num);
				}
			}
		}
		return matches;
	}
	public void checkEquidistant(List<Integer> matches){
		HashSet<Integer> distances = new HashSet<>();
		for(int cnt1 = 0;cnt1 < matches.size();++cnt1){
			for(int cnt2 = cnt1 + 1;cnt2 < matches.size();++cnt2){
				int num = matches.get(cnt2) - matches.get(cnt1);
				if((MIN_NUMBER + num + num) <= MAX_NUMBER){
					distances.add(num);
				}
			}
		}
		for(Integer distance : distances){
			for(Integer match : matches){
				if(matches.contains(match + distance) && (matches.contains(match + distance + distance))){
					concatenationOfNumbers = match.toString() + Integer.toString(match + distance) + Integer.toString(match + distance + distance);
				}
				if(!concatenationOfNumbers.isBlank()){
					break;
				}
			}
			if(!concatenationOfNumbers.isBlank()){
				break;
			}
		}
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		concatenationOfNumbers = "";
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The 12-digit number formed by concatenation the three terms in the sequence is %s", concatenationOfNumbers);
	}
	//Returns the concatenation of numbers
	public String getConcatNums(){
		solvedCheck("concatenation of numbers");
		return concatenationOfNumbers;
	}
}

/* Results:
The 12-digit number formed by concatenation the three terms in the sequence is 296962999629
It took an average of 4.261 milliseconds to run this problem through 100 iterations
*/
