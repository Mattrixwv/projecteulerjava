//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem37.java
//Matthew Ellison
// Created: 10-11-21
//Modified: 06-30-23
//What is the largest 1-9 pandigital number that can be formed as the concatenated product of an integer with 1, 2, ... n where n > 1
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.StringAlgorithms;


public class Problem38 extends Problem{
	//Variables
	//Static variables
	private static final long HIGHEST_POSSIBLE_NUM = 9999;	//The highest number that needs to be checked for a 1-9 pandigital
	//Instance variables
	protected long largestNum;	//The number passed to the executeFormula function that returns the largest pandigital
	protected long pandigital;	//The largest pandigital number found

	//Functions
	//Constructor
	public Problem38(){
		super("What is the largest 1-9 pandigital number that can be formed as the concatenated product of an integer with 1, 2, ... n where n > 1");
		largestNum = 0;
		pandigital = 0;
	}
	//Operational functions
	//Take the number and add its multiples to a string to return
	private String executeFormula(int num){
		//Turn the current number into a string
		StringBuilder numStr = new StringBuilder();
		numStr.append(Integer.toString(num));
		int cnt = 2;
		//Multiply the number and append the product to the string until you have one long enough
		do{
			numStr.append(Integer.toString(num * cnt));
			++cnt;
		}while(numStr.length() < 9);

		return numStr.toString();
	}
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Loop from 1 -> HIGHEST_POSSIBLE_NUM checking for pandigitals
		for(int cnt = 1;cnt <= HIGHEST_POSSIBLE_NUM;++cnt){
			//Get the string from the formula
			String numStr = executeFormula(cnt);
			long panNum = Long.parseLong(numStr);
			//If the number is pandigital save it as the highest number
			if(StringAlgorithms.isPandigital(numStr) && (panNum > pandigital)){
				largestNum = cnt;
				pandigital = panNum;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the prblem so it can be run again
	@Override
	public void reset(){
		super.reset();
		largestNum = 0;
		pandigital = 0;
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The largest appended product pandigital is %d", pandigital);
	}
	//Returns the largest number
	public long getLargestNum(){
		solvedCheck("largest number");
		return largestNum;
	}
	//Returns the pandigital of the number
	public long getPandigital(){
		solvedCheck("pandigital");
		return pandigital;
	}
}

/* Results:
The largest appended product pandigital is 932718654
It took 16.947 milliseconds to solve this problem.
*/
