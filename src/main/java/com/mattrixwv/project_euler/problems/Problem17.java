//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem17.java
//Matthew Ellison
// Created: 03-04-19
//Modified: 06-27-23
//If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


public class Problem17 extends Problem{
	//Variables
	//Static variables
	private static final int START_NUM = 1;		//This is the smallest number to get the words of
	private static final int STOP_NUM = 1000;	//This is the largest number to get the words of
	//Instance variables
	protected long letterCount;	//This is the cumulative number of letters in the words of the numbers

	//Functions
	//Constructor
	public Problem17(){
		super(String.format("If all the numbers from %d to %d inclusive were written out in words, how many letters would be used?", START_NUM, STOP_NUM));
		letterCount = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start with 1 and increment
		for(int num = START_NUM;num <= STOP_NUM;++num){
			//Pass the number to a function that will create a string for the number
			String currentNumString = makeWordFromNum(num);
			//Pass the string to the function that will count the number of letters in it, ignoring whitespace and punctuation and add that number to the running tally
			letterCount += getNumberChars(currentNumString);
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		letterCount = 0;
	}
	//This function makes a word out of the number passed into it
	protected String makeWordFromNum(int number){
		int num = number;
		StringBuilder numberString = new StringBuilder();
		//Starting with the largest digit create a string based on the number passed in
		//Check for negative
		if(num < 0){
			numberString.append("negative ");
			num = -num;
		}

		//Check if the number is zero
		if(num == 0){
			numberString.append("zero");
		}

		//Start with the thousands place
		if((num / 1000D) >= 1D){
			numberString.append(makeWordFromNum((int)Math.floor(num / 1000D)));
			numberString.append(" thousand");
			num -= ((int)Math.floor(num / 1000D) * 1000);
		}

		//Check the hundreds place
		if((num / 100D) >= 1D){
			numberString.append(makeWordFromNum((int)Math.floor(num / 100D)));
			numberString.append(" hundred");
			num -= ((int)Math.floor(num / 100D) * 100);
		}

		//Insert an and if there is need
		if(!numberString.toString().isBlank() && (numberString.indexOf("negative") != 0) && (num > 0)){
			numberString.append(" and ");
		}

		//Check for tens place
		if((num / 10D) >= 2D){
			//For the tens you need to do something special
			int tensPlace = (int)Math.floor(num / 10D);
			switch(tensPlace){
				case 9: numberString.append("ninety"); break;
				case 8: numberString.append("eighty"); break;
				case 7: numberString.append("seventy"); break;
				case 6: numberString.append("sixty"); break;
				case 5: numberString.append("fifty"); break;
				case 4: numberString.append("forty"); break;
				case 3: numberString.append("thirty"); break;
				case 2: numberString.append("twenty"); break;
			}
			num -= (tensPlace * 10);
			//If there is something left in the number you will need a dash to separate the tens and ones place
			if(num > 0){
				numberString.append("-");
			}
		}
		//Check for teens
		else if((num / 10D) >= 1D){
			int onesPlace = (num % 10);
			switch(onesPlace){
				case 9: numberString.append("nineteen"); break;
				case 8: numberString.append("eighteen"); break;
				case 7: numberString.append("seventeen"); break;
				case 6: numberString.append("sixteen"); break;
				case 5: numberString.append("fifteen"); break;
				case 4: numberString.append("fourteen"); break;
				case 3: numberString.append("thirteen"); break;
				case 2: numberString.append("twelve"); break;
				case 1: numberString.append("eleven"); break;
				case 0: numberString.append("ten"); break;
			}
			//If this was hit the number was completed
			num = 0;
		}

		//Check for the ones place
		if(num >= 1){
			switch(num){
				case 9: numberString.append("nine"); break;
				case 8: numberString.append("eight"); break;
				case 7: numberString.append("seven"); break;
				case 6: numberString.append("six"); break;
				case 5: numberString.append("five"); break;
				case 4: numberString.append("four"); break;
				case 3: numberString.append("three"); break;
				case 2: numberString.append("two"); break;
				case 1: numberString.append("one"); break;
			}
			//If this was hit the number was completed
			num = 0;
		}

		//Return the string
		return numberString.toString();
	}
	//This counts the number of letters in the string that is passed in (ignoring numbers and punctuation)
	private int getNumberChars(String number){
		int sumOfLetters = 0;
		//Start at location 0 and count the number of letters, ignoring punctuation and whitespace
		for(int location = 0;location < number.length();++location){
			if(Character.isAlphabetic(number.charAt(location))){
				sumOfLetters += 1;
			}
		}

		//Return the number of letters
		return sumOfLetters;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all the letters in all the numbers %d-%d is %d", START_NUM, STOP_NUM, letterCount);
	}
	//Returns the number of letters asked for
	public long getLetterCount(){
		solvedCheck("letter count");
		return letterCount;
	}
}


/* Results:
The sum of all the letters in all the numbers 1-1000 is 21124
It took an average of 216.210 microseconds to run this problem through 100 iterations
*/
