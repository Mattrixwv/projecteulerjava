//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem3.java
//Matthew Ellison
// Created: 03-01-19
//Modified: 06-27-23
//The largest prime factor of 600851475143
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.NumberAlgorithms;
import com.mattrixwv.exceptions.InvalidResult;


public class Problem3 extends Problem{
	//Variables
	//Static variables
	private static final long GOAL_NUMBER = 600851475143L;	//The number that needs factored
	//Instance variables
	protected List<Long> factors;	//Holds the factors of goalNumber

	//Functions
	//Constructor
	public Problem3(){
		super(String.format("What is the largest prime factor of %d?", GOAL_NUMBER));
		factors = new ArrayList<>();
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve() throws InvalidResult{
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get all the factors of the number
		factors = NumberAlgorithms.getFactors(GOAL_NUMBER);
		//The last element should be the largest factor


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		factors.clear();
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The largest factor of the number %d is %d", GOAL_NUMBER, factors.get(factors.size() - 1));
	}
	//Returns the list of factors of the number
	public List<Long> getFactors(){
		solvedCheck("factors");
		return factors;
	}
	//Returns the largest factor of the number
	public long getLargestFactor(){
		solvedCheck("largest factor");
		return factors.get(factors.size() - 1);
	}
	//Returns the number for which we are getting the factor
	public static long getGoalNumber(){
		return GOAL_NUMBER;
	}
}


/* Results:
The largest factor of the number 600851475143 is 6857
It took an average of 48.209 milliseconds to run this problem through 100 iterations
*/
