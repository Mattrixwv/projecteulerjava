//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem22.java
//Matthew Ellison
// Created: 03-20-19
//Modified: 06-27-23
//What is the total of all the name scores in this file?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.ArrayAlgorithms;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Problem22 extends Problem{
	//Variables
	//Static variables
	protected static String fileName = "files/Problem22Names.txt";
	//Holds the names that will be scored
	protected static ArrayList<String> names = new ArrayList<>();
	//Instance variables
	protected ArrayList<Long> sums;	//Holds the score based on the sum of the characters in the name
	protected ArrayList<Long> prod;	//Holds the score based on the sum of the characters and the location in alphabetical order
	protected long sum;	//Holds the sum of the scores

	//Functions
	//Constructor
	public Problem22(){
		super("What is the total of all the name scores in this file?");
		sums = new ArrayList<>();
		prod = new ArrayList<>();
		sum = 0;
	}
	//Operational functions
	//Read words from file into array
	private void readFile(){
		if(!names.isEmpty()){
			return;
		}
		File file = new File(fileName);
		if(file.exists()){
			try{
				List<String> lines = Files.readAllLines(file.toPath());
				for(String line : lines){
					String[] words = line.split(",");
					for(String word : words){
						names.add(word.replace("\"", ""));
					}
				}
			}
			catch(IOException error){
				throw new InvalidParameterException("Error reading file", error);
			}
		}
		else{
			throw new InvalidParameterException("File not found");
		}
	}
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Read the file into the array
		readFile();
		//Sort all the names
		Collections.sort(names);
		//Step through every name adding up the values of the characters
		for(int nameCnt = 0;nameCnt < names.size();++nameCnt){
			//Step through every character in the current name adding up the value of the characters
			sums.add(0L);
			for(int charCnt = 0;charCnt < names.get(nameCnt).length();++charCnt){
				//A = 65 so subtracting 64 means A - 1. This will only work correctly if all letters are capitalized
				sums.set(nameCnt, sums.get(nameCnt) + names.get(nameCnt).charAt(charCnt) - 64);
			}
		}
		//Get the product for all numbers
		for(int cnt = 0;cnt < sums.size();++cnt){
			prod.add(sums.get(cnt) * (cnt + 1));
		}

		//Get the sum of all the numbers
		sum = ArrayAlgorithms.getLongSum(prod);

		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		sums.clear();
		prod.clear();
		sum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The answer to the question is %d", sum);
	}
	//Returns the vector of the names being scored
	public static List<String> getNames(){
		return names;
	}
	//Returns the sum of the names scores
	public long getNameScoreSum(){
		solvedCheck("score of the names");
		return sum;
	}
}


/* Results:
The answer to the question is 871198282
It took an average of 2.609 milliseconds to run this problem through 100 iterations
*/
