//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem41.java
//Mattrixwv
// Created: 08-20-22
//Modified: 06-30-23
//What is the largest n-digit pandigital prime?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;

import com.mattrixwv.NumberAlgorithms;
import com.mattrixwv.StringAlgorithms;


public class Problem41 extends Problem{
	//Variables
	//Instance variables
	protected long largestPrimePandigital;

	//Functions
	//Constructor
	public Problem41(){
		super("What is the largest n-digit pandigital prime?");
		largestPrimePandigital = 0;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get all of the possible numbers that could be pandigital
		ArrayList<String> perms = new ArrayList<>();
		perms.addAll(StringAlgorithms.getPermutations("1"));
		perms.addAll(StringAlgorithms.getPermutations("12"));
		perms.addAll(StringAlgorithms.getPermutations("123"));
		perms.addAll(StringAlgorithms.getPermutations("1234"));
		perms.addAll(StringAlgorithms.getPermutations("12345"));
		perms.addAll(StringAlgorithms.getPermutations("123456"));
		perms.addAll(StringAlgorithms.getPermutations("1234567"));
		perms.addAll(StringAlgorithms.getPermutations("12345678"));
		perms.addAll(StringAlgorithms.getPermutations("123456789"));
		//Check if the numbers are prime
		for(String perm : perms){
			long num = Long.parseLong(perm);
			if((NumberAlgorithms.isPrime(num)) && (num > largestPrimePandigital)){
				largestPrimePandigital = num;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		largestPrimePandigital = 0;
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The largest n-digit pandigital prime is %d", largestPrimePandigital);
	}
	//Returns the largest prime pandigital
	public long getLargestPrimePandigital(){
		solvedCheck("largest prime pandigital");
		return largestPrimePandigital;
	}
}

/* Results:
The largest n-digit pandigital prime is 7652413
It took an average of 108.232 milliseconds to run this problem through 100 iterations
*/
