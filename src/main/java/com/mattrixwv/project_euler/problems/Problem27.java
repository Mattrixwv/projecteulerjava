//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem27.java
//Matthew Ellison
// Created: 09-15-19
//Modified: 06-27-23
//Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.NumberAlgorithms;


public class Problem27 extends Problem{
	//Variables
	//Static varibles
	private static final int LARGEST_POSSIBLE_A = 999;
	private static final int LARGEST_POSSIBLE_B = 1000;
	//Instance variables
	protected int topA;	//The A for the most n's generated
	protected int topB;	//The B for the most n's generated
	protected int topN;	//The most n's generated

	//Functions
	//Constructor
	public Problem27(){
		super(String.format("Find the product of the coefficients, |a| <= %d and |b| <= %d, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0", LARGEST_POSSIBLE_A, LARGEST_POSSIBLE_B));
		topA = 0;
		topB = 0;
		topN = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start with the lowest possible A and check all possibilities after that
		for(int a = -LARGEST_POSSIBLE_A;a <= LARGEST_POSSIBLE_A;++a){
			//Start with the lowest possible B and check all possibilities after that
			for(int b = -LARGEST_POSSIBLE_B;b <= LARGEST_POSSIBLE_B;++b){
				//Start with n=0 and check the formula to see how many primes you can get with concecutive n's
				int n = 0;
				int quadratic = (n * n) + (a * n) + b;
				while(NumberAlgorithms.isPrime(quadratic)){
					++n;
					quadratic = (n * n) + (a * n) + b;
				}
				--n;	//Negate an n because the last formula failed

				//Set all the largest numbers if this created more primes than any other
				if(n > topN){
					topN = n;
					topB = b;
					topA = a;
				}
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		topA = 0;
		topB = 0;
		topN = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The greatest number of primes found is %d%nIt was found with A = %d, B = %d%nThe product of A and B is %d", topN, topA, topB, topA * topB);
	}
	//Returns the top A that was generated
	public int getTopA(){
		solvedCheck("largest A");
		return topA;
	}
	//Returns the top B that was generated
	public int getTopB(){
		solvedCheck("largest B");
		return topB;
	}
	//Returns the top N that was generated
	public int getTopN(){
		solvedCheck("largest N");
		return topN;
	}
	//Resuts the product of A and B for the answer
	public int getProduct(){
		solvedCheck("product of A and B");
		return topA * topB;
	}
}


/* Results:
The greatest number of primes found is 70
It was found with A = -61, B = 971
The product of A and B is -59231
It took an average of 18.835 milliseconds to run this problem through 100 iterations
*/
