//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem5.java
//Matthew Ellison
// Created: 03-01-19
//Modified: 06-27-23
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


public class Problem5 extends Problem{
	//Variables
	//Instance variables
	protected int smallestNum;	//The smallest number that is found

	//Functions
	//Constructor
	public Problem5(){
		super("What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?");
		smallestNum = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start at 20 because it must at least be divisible by 20. Increment by 2 because it must be an even number to be divisible by 2
		boolean numFound = false;	//A flag for finding the divisible number
		int currentNum = 20;		//The number that it is currently checking against
		while((currentNum > 0) && (!numFound)){
			//Start by assuming you found the number (because we throw a flag if we didn't find it)
			numFound = true;
			//Step through every number from 1-20 seeing if the current number is divisible by it
			for(int divisor = 1;divisor <= 20;++divisor){
				//If it is not divisible then throw a flag and start looking at the next number
				if((currentNum % divisor) != 0){
					numFound = false;
					break;
				}
			}
			//If you didn't find the correct number then increment by 2
			if(!numFound){
				currentNum += 2;
			}
		}
		//Save the current number as the smallest
		smallestNum = currentNum;


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		smallestNum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The smallest positive number evenly divisible by all numbers 1-20 is %d", smallestNum);
	}
	//Returns the requested number
	public int getNumber(){
		solvedCheck("number");
		return smallestNum;
	}
}


/* Results:
The smallest positive number evenly divisible by all numbers 1-20 is 232792560
It took an average of 213.942 milliseconds to run this problem through 100 iterations
*/
