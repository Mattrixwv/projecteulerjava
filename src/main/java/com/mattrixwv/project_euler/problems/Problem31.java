//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem31.java
//Matthew Ellison
// Created: 06-19-20
//Modified: 06-27-23
//How many different ways can £2 be made using any number of coins?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


public class Problem31 extends Problem{
	//Variables
	//Static variables
	private static final int DESIRED_VALUE = 200;	//The value of coins we want
	//Instance variables
	protected int permutations;	//The number of permutations that are found

	//Functions
	//Constructor
	public Problem31(){
		super("How many different ways can 2 pounds be made using any number of coins?");
		permutations = 0;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start with 200p and remove the necessary coins with each loop
		for(int pound2 = DESIRED_VALUE; pound2 >= 0;pound2 -= 200){
			for(int pound1 = pound2;pound1 >= 0;pound1 -= 100){
				for(int pence50 = pound1;pence50 >= 0;pence50 -= 50){
					for(int pence20 = pence50;pence20 >= 0;pence20 -= 20){
						for(int pence10 = pence20;pence10 >= 0;pence10 -= 10){
							for(int pence5 = pence10;pence5 >= 0;pence5 -= 5){
								for (int pence2 = pence5; pence2 >= 0; pence2 -= 2){
									++permutations;
								}
							}
						}
					}
				}
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		permutations = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("There are %d ways to make 2 pounds with the given denominations of coins", permutations);
	}
	//Returns the number of correct permutations of the coins
	public int getPermutations(){
		solvedCheck("number of correct permutations of the coins");
		return permutations;
	}
}


/* Results:
There are 73682 ways to make 2 pounds with the given denominations of coins
It took an average of 19.175 microseconds to run this problem through 100 iterations
*/
