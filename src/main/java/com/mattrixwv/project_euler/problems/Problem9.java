//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem9.java
//Matthew Ellison
// Created: 03-02-19
//Modified: 06-27-23
//There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.project_euler.exceptions.Unsolved;


public class Problem9 extends Problem{
	//Variables
	//Static variables
	protected int goalSum = 1000;	//The number that we want the sum of a, b, and c to equal
	//Instance variables
	protected int a;	//The size of the first side
	protected int b;	//The size of the second side
	protected double c;	//The size of the hyp
	protected boolean found;	//A flag to determine if we have found the solution yet

	//Functions
	//Constructor
	public Problem9(){
		super(String.format("There exists exactly one Pythagorean triplet for which a + b + c = %d. Find the product abc.", 1000));
		a = 1;
		b = 0;
		c = 0;
		found = false;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Loop through all possible a's
		while((a < goalSum) && !found){
			b = a + 1;	//b must be larger than a
			c = Math.sqrt((a * a) + (double)(b * b));	//Compute the hyp
	
			//Loop through all possible b's for this a
			while((a + b + c) < goalSum){
				++b;
				c = Math.sqrt((a * a) + (double)(b * b));
			}

			//If the sum == 1000 you found the number, otherwise go to the next possible a
			if((a + b + c) == goalSum){
				found = true;
			}
			else{
				++a;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		if(found){
			solved = true;
		}
		else{
			throw new Unsolved("The problem was not solved!");
		}
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		a = 1;
		b = 0;
		c = 0;
		found = false;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The Pythagorean triplet is %d + %d + %d%nThe numbers' product is %d", a, b, Math.round(c), a * b * Math.round(c));
	}
	//Returns the length of the first side
	public int getSideA(){
		solvedCheck("first side");
		return a;
	}
	//Returns the length of the second side
	public int getSideB(){
		solvedCheck("second side");
		return b;
	}
	//Returns the length of the hyp
	public int getSideC(){
		solvedCheck("third side");
		return (int)c;
	}
	//Returns the product of the 3 sides
	public int getProduct(){
		solvedCheck("product of all three sides");
		return a * b * (int)c;
	}
}


/* Results:
The Pythagorean triplet is 200 + 375 + 425
The numbers' product is 31875000
It took an average of 380.920 microseconds to run this problem through 100 iterations
*/
