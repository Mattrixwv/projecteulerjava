//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem24.java
//Matthew Ellison
// Created: 03-24-19
//Modified: 06-27-23
//What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.StringAlgorithms;

import java.util.ArrayList;
import java.util.List;


public class Problem24 extends Problem{
	//Variables
	//Static variables
	protected static int neededPerm = 1000000;	//The number of the permutation that you need
	protected static String nums = "0123456789";	//All of the characters that we need to get the permutations of
	//Instance variables
	protected List<String> permutations;	//Holds all of the permutations of the string nums

	//Functions
	//Constructor
	public Problem24(){
		super(String.format("What is the %dth lexicographic permutation of the digits %s?", neededPerm, nums));
		permutations = new ArrayList<>();
	}
	//Operational functions
	//Solve the problems
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get all the permutations of the string
		permutations = StringAlgorithms.getPermutations(nums);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		permutations.clear();
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The %dth permutation is %s", neededPerm, permutations.get(neededPerm - 1));
	}
	//Returns an ArrayList with all of the permutations
	public List<String> getPermutationsList(){
		solvedCheck("permutations");
		return permutations;
	}
	//Returns the requested permutation
	public String getPermutation(){
		solvedCheck("1,000,000th permutation");
		return permutations.get(neededPerm - 1);
	}
}


/* Results
The 1 millionth permutation is 2783915460
It took an average of 1.140 seconds to run this problem through 100 iterations
*/
