//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem16.java
//Matthew Ellison
// Created: 03-04-19
//Modified: 06-27-23
//What is the sum of the digits of the number 2^1000?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.math.BigInteger;


public class Problem16 extends Problem{
	//Variables
	//Static variables
	private static final int NUM_TO_POWER = 2;	//The number that is going to be raised to a power
	private static final int POWER = 1000;	//The power that the number is going to be raised to
	//Instance variables
	protected BigInteger num;		//The number to be calculated
	protected int sumOfElements;	//The sum of all digits in the number

	//Functions
	//Constructor
	public Problem16(){
		super(String.format("What is the sum of the digits of the number %d^%d?", NUM_TO_POWER, POWER));
		num = BigInteger.ZERO;
		sumOfElements = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get the number
		num = BigInteger.valueOf(NUM_TO_POWER).pow(POWER);

		//Get a string of the number
		String numString = num.toString();

		//Add up the individual characters of the string
		for(int cnt = 0;cnt < numString.length();++cnt){
			sumOfElements += Integer.parseInt(numString.substring(cnt, cnt + 1));
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		num = BigInteger.ZERO;
		sumOfElements = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("%d^%d = %s%nThe sum of the elements is %d", NUM_TO_POWER, POWER, num.toString(), sumOfElements);
	}
	//Returns the number that was calculated
	public BigInteger getNumber(){
		solvedCheck("number");
		return num;
	}
	//Return the sum of the digits of the number
	public int getSum(){
		solvedCheck("sum");
		return sumOfElements;
	}
}


/* Results:
2^1000 = 10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376
The sum of the elements is 1366
It took an average of 60.914 microseconds to run this problem through 100 iterations
*/
