//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem45.java
//Mattrixwv
// Created: 08-20-22
//Modified: 06-30-23
//If p is the perimeter of a right triangle for which value of p <= 1000 is the number of solutions for the sides {a, b, c} maximized
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;

import com.mattrixwv.generators.HexagonalNumberGenerator;
import com.mattrixwv.generators.PentagonalNumberGenerator;
import com.mattrixwv.generators.TriangularNumberGenerator;

public class Problem45 extends Problem{
	//Variables
	//Static variables
	private static final long MIN_NUMBER = 40756;
	//Instance variables
	protected long num;
	protected TriangularNumberGenerator triGen;
	protected PentagonalNumberGenerator penGen;
	protected HexagonalNumberGenerator hexGen;

	//Functions
	//Constructor
	public Problem45(){
		super("Find the next triangle number after 40755 that is also pentagonal and hexagonal");
		num = 0;
		triGen = new TriangularNumberGenerator();
		penGen = new PentagonalNumberGenerator();
		hexGen = new HexagonalNumberGenerator();
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the porblem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();

		//Generate the next number after MIN_NUMBER
		long triNum = 0;
		while(triNum < MIN_NUMBER){
			triNum = triGen.next();
		}
		long penNum = 0;
		while(penNum < MIN_NUMBER){
			penNum = penGen.next();
		}
		long hexNum = 0;
		while(hexNum < MIN_NUMBER){
			hexNum = hexGen.next();
		}
		//Generate the next numbers until they all match
		boolean matchFound = false;
		while(!matchFound){
			hexNum = hexGen.next();
			while(penNum < hexNum){
				penNum = penGen.next();
			}
			while(triNum < hexNum){
				triNum = triGen.next();
			}

			if((hexNum == penNum) && (hexNum == triNum)){
				num = hexNum;
				matchFound = true;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the porblem so it can be run again
	@Override
	public void reset(){
		super.reset();
		num = 0;
		triGen = new TriangularNumberGenerator();
		penGen = new PentagonalNumberGenerator();
		hexGen = new HexagonalNumberGenerator();
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The next triangular/pentagonal/hexagonal number is %d", num);
	}
	//Returns the number
	public long getNum(){
		solvedCheck("triangular/pentagonal/hexagonal number");
		return num;
	}
}

/* Results:
The next triangular/pentagonal/hexagonal number is 1533776805
It took an average of 589.688 microseconds to run this problem through 100 iterations
*/
