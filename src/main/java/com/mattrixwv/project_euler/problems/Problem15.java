//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem15.java
//Matthew Ellison
// Created: 03-04-19
//Modified: 06-27-23
//How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


public class Problem15 extends Problem{
	//Variables
	//Static vaiables
	protected static int gridWidth = 20;	//The width of the box to traverse
	protected static int gridLength = 20;	//The height of the box to traverse
	//Instance variables
	protected long numOfRoutes;	//The number of routes from 0, 0 to 20, 20

	//Functions
	//Constructor
	public Problem15(){
		super(String.format("How many routes from the top left corner to the bottom right corner are there through a %dx%d grid if you can only move right and down?", gridWidth, gridLength));
		numOfRoutes = 0;
	}
	//Operational functions
	//Solve the problems
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//We write this as a recursive function
		//When in a location it always moves right first, then down
		move(0, 0);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//This function acts as a handler for moving the position on the grid and counting the distance
	//It moves right first, then down
	private void move(int currentX, int currentY){
		//Check if you are at the end and act accordingly
		if((currentX == gridWidth) && (currentY == gridLength)){
			++numOfRoutes;
			return;
		}

		//Move right if possible
		if(currentX < gridWidth){
			move(currentX + 1, currentY);
		}

		//Move down if possible
		if(currentY < gridLength){
			move(currentX, currentY + 1);
		}
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		numOfRoutes = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The number of routes is %d", numOfRoutes);
	}
	//Returns the number of routes found
	public long getNumberOfRoutes(){
		solvedCheck("number of routes");
		return numOfRoutes;
	}
}


/* Results:
The number of routes is 137846528820
It took an average of 20.702 minutes to run this problem through 10 iterations
*/
