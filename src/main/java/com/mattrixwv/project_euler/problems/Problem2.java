//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem2.java
//Matthew Ellison
// Created: 03-01-19
//Modified: 06-27-23
//The sum of the even Fibonacci numbers less than 4,000,000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.List;

import com.mattrixwv.NumberAlgorithms;


public class Problem2 extends Problem{
	//Variables
	//Static variables
	private static final int TOP_NUM = 4000000 - 1;	//The largest number that will be checked as a fibonacci number
	//Instance variables
	protected int fullSum;	//Holds the sum of all the numbers

	//Functions
	//Constructor
	public Problem2(){
		super(String.format("What is the sum of the even Fibonacci numbers less than %d?", (TOP_NUM + 1)));
		fullSum = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get a list of all fibonacci numbers <= TOP_NUM
		List<Integer> fibNums = NumberAlgorithms.getAllFib(TOP_NUM);
		//Step through every element in the list checking if it is even
		for(int num : fibNums){
			//If the number is even add it to the running tally
			if((num % 2) == 0){
				fullSum += num;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		fullSum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all even fibonacci numbers <= %d is %d", TOP_NUM, fullSum);
	}
	//Returns the requested sum
	public int getSum(){
		solvedCheck("sum");
		return fullSum;
	}
}


/* Results:
The sum of all even fibonacci numbers <= 3999999 is 4613732
It took an average of 29.713 microseconds to run this problem through 100 iterations
*/
