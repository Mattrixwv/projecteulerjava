//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem36.java
//Matthew Ellison
// Created: 06-29-21
//Modified: 06-30-23
//Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.ArrayAlgorithms;
import com.mattrixwv.NumberAlgorithms;
import com.mattrixwv.StringAlgorithms;


public class Problem36 extends Problem{
	//Variables
	//Static variables
	private static final int MAX_NUM = 999999;	//The largest number that will be checked
	//Instance variables
	protected ArrayList<Integer> palindromes;	//All numbers that are palindromes in base 10 and 2
	protected int sum;	//The sum of all elements in the list of palindromes

	//Functions
	//Constructor
	public Problem36(){
		super("Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.");
		palindromes = new ArrayList<>();
		sum = 0;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start with 1, check if it is a palindrome in base 10 and 2, and continue to MAX_NUM
		for(int num = 1;num < MAX_NUM;++num){
			//Check if num is a palindrome
			if(StringAlgorithms.isPalindrome(Integer.toString(num))){
				//Convert num to base 2 and see if that is a palindrome
				String binNum = NumberAlgorithms.toBin(num);
				if(StringAlgorithms.isPalindrome(binNum)){
					//Add num to the list of palindromes
					palindromes.add(num);
				}
			}
		}
		//Get the sum of all palindromes in the list
		sum = ArrayAlgorithms.getSum(palindromes);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		palindromes.clear();
		sum = 0;
	}
	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all base 10 and base 2 palindromic numbers < %d is %d", MAX_NUM, sum);
	}
	//Return the List of palindromes < MAX_NUM
	public List<Integer> getPalindromes(){
		solvedCheck("list of palindromes");
		return palindromes;
	}
	//Return the sum of all elements in the List of palindromes
	public int getSumOfPalindromes(){
		solvedCheck("sum of all palindromes");
		return sum;
	}
}


/* Results:
The sum of all base 10 and base 2 palindromic numbers < 999999 is 872187
It took an average of 39.606 milliseconds to run this problem through 100 iterations
*/
