//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem8.java
//Matthew Ellison
// Created: 03-28-19
//Modified: 06-27-23
//Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.List;

public class Problem8 extends Problem{
	//Variables
	//Static variables
	//The 1000 digit number to check
	protected static String fileName = "files/Problem8Number.txt";
	protected static String number;
	//Instance variables
	protected String maxNums;	//Holds the string of the largest product
	protected long maxProduct;	//Holds the largest product of 13 numbers

	//Functions
	//Constructor
	public Problem8(){
		super("Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?");
		maxNums = "";
		maxProduct = 0;
	}
	//Operational functions
	//Read number from file into number
	private static void readFile(){
		if(number != null){
			return;
		}
		File file = new File(fileName);
		if(file.exists()){
			try{
				List<String> lines = Files.readAllLines(file.toPath());
				number = lines.get(0);
			}
			catch(IOException error){
				throw new InvalidParameterException("Error reading file", error);
			}
		}
		else{
			throw new InvalidParameterException("Error opening file");
		}
	}
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Read the file into the number
		readFile();

		//Start the timer
		timer.start();


		//Cycle through the string of numbers looking for the maximum product
		for(int cnt = 12;cnt < number.length();++cnt){
			long currentProduct = Long.parseLong(number.substring(cnt - 12, cnt - 11)) * Long.parseLong(number.substring(cnt - 11, cnt - 10)) * Long.parseLong(number.substring(cnt - 10, cnt - 9)) * Long.parseLong(number.substring(cnt - 9, cnt - 8)) * Long.parseLong(number.substring(cnt - 8, cnt - 7)) * Long.parseLong(number.substring(cnt - 7, cnt - 6)) * Long.parseLong(number.substring(cnt - 6, cnt - 5)) * Long.parseLong(number.substring(cnt - 5, cnt - 4)) * Long.parseLong(number.substring(cnt - 4, cnt - 3)) * Long.parseLong(number.substring(cnt - 3, cnt - 2)) * Long.parseLong(number.substring(cnt - 2, cnt - 1)) * Long.parseLong(number.substring(cnt - 1, cnt)) * Long.parseLong(number.substring(cnt, cnt + 1));

			//Check if the product is greater than the current maximum
			if(currentProduct > maxProduct){
				maxNums = number.substring(cnt - 12, cnt + 1);
				maxProduct = currentProduct;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		maxNums = "";
		maxProduct = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The greatest product is %d%nThe numbers are %s", maxProduct, maxNums);
	}
	//Returns the string of numbers that produces the largest product
	public String getLargestNums(){
		solvedCheck("numbers that make the largest product");
		return maxNums;
	}
	//Returns the requested product
	public long getLargestProduct(){
		solvedCheck("product of the numbers");
		return maxProduct;
	}
}


/* Results:
The greatest product is 23514624000
The numbers are 5576689664895
It took an average of 924.880 microseconds to run this problem through 100 iterations
*/
