//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem37.java
//Matthew Ellison
// Created: 07-01-21
//Modified: 06-30-23
//Find the sum of the only eleven primes that are both truncatable from left to right and right to left (2, 3, 5, and 7 are not counted).
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.ArrayAlgorithms;
import com.mattrixwv.NumberAlgorithms;
import com.mattrixwv.generators.SieveOfEratosthenes;


public class Problem37 extends Problem{
	//Variables
	//Static variables
	private static final long LAST_PRIME_BEFORE_CHECK = 7;	//The last prime before 11 since single digit primes aren't checked
	//Instance variables
	protected List<Long> truncPrimes;	//All numbers that are truncatable primes
	protected long sum;	//The sum of all elements in truncPrimes

	//Functions
	//Constructor
	public Problem37(){
		super("Find the sum of the only eleven primes that are both truncatable from left to right and right to left (2, 3, 5, and 7 are not counted).");
		truncPrimes = new ArrayList<>();
		sum = 0;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Create the sieve and get the first prime number
		SieveOfEratosthenes sieve = new SieveOfEratosthenes();
		long currentPrime = sieve.next();
		//Loop through the sieve until you get to LAST_PRIME_BEFORE_CHECK
		while(currentPrime < LAST_PRIME_BEFORE_CHECK){
			currentPrime = sieve.next();
		}
		//Loop until truncPrimes contains 11 elements
		while(truncPrimes.size() < 11){
			//Get the next prime
			currentPrime = sieve.next();
			//Convert the prime to a string
			String primeString = Long.toString(currentPrime);
			//If the string contains an even digit move to the next prime
			boolean isTruncPrime = containsEvenDigits(primeString);
			//Start removing digits from the left and see if the number stays prime
			if(isTruncPrime){
				isTruncPrime = removeLeft(primeString);
			}
			//Start removing digits from the right and see if the number stays prime
			if(isTruncPrime){
				isTruncPrime = removeRight(primeString);
			}
			//If the number remained prime through all operations add it to the vector
			if(isTruncPrime){
				truncPrimes.add(currentPrime);
			}
		}
		//Get the sum of all elements in the truncPrimes vector
		sum = ArrayAlgorithms.getLongSum(truncPrimes);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Check if the string contains any digits that are even
	private boolean containsEvenDigits(String primeString){
		boolean isTruncPrime = true;
		for(int strLoc = 0;(strLoc < primeString.length()) && (isTruncPrime);++strLoc){
			//Allow 2 to be the first digit
			if((strLoc == 0) && (primeString.charAt(strLoc) == '2')){
				continue;
			}
			switch(primeString.charAt(strLoc)){
				case '0', '2', '4', '6', '8' : isTruncPrime = false; break;
				default: break;
			}
		}
		return isTruncPrime;
	}
	//Check if the number remains prime when you remove numbers from the left
	private boolean removeLeft(String primeString){
		boolean isTruncPrime = true;
		for(int truncLoc = 1;truncLoc < primeString.length();++truncLoc){
			//Create a substring of the prime, removing the needed digits from the left
			String primeSubstring = primeString.substring(truncLoc);
			//Convert the string to an int and see if the number is still prime
			long newPrime = Long.parseLong(primeSubstring);
			if(!NumberAlgorithms.isPrime(newPrime)){
				isTruncPrime = false;
				break;
			}
		}
		return isTruncPrime;
	}
	//Check if the number remains prime when you remove numbers from the right
	private boolean removeRight(String primeString){
		boolean isTruncPrime = true;
		for(int truncLoc = 1;truncLoc < primeString.length();++truncLoc){
			//Create a substring of the prime, removing the needed digits from the right
			String primeSubstring = primeString.substring(0, primeString.length() - truncLoc);
			//Convert the string to an int and see if the number is still prime
			long newPrime = Long.parseLong(primeSubstring);
			if(!NumberAlgorithms.isPrime(newPrime)){
				isTruncPrime = false;
				break;
			}
		}
		return isTruncPrime;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		truncPrimes.clear();
		sum = 0;
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all left and right truncatable primes is %d", sum);
	}
	//Returns the list of primes that can be truncated
	public List<Long> getTruncatablePrimes(){
		solvedCheck("list of truncatable primes");
		return truncPrimes;
	}
	//Return the sum of all primes in truncPrimes
	public long getSumOfPrimes(){
		solvedCheck("sum of truncatable primes");
		return sum;
	}
}


/* Results:
The sum of all left and right truncatable primes is 748317
It took an average of 103.829 milliseconds to run this problem through 100 iterations
*/
