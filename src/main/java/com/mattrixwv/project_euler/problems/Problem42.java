//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem42.java
//Mattrixwv
// Created: 08-20-22
//Modified: 06-30-23
//Triangular number t(n) = (n * (n + 1)) / 2. For A = 1, B = 2, ... find the number of triangular words in the file
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;


public class Problem42 extends Problem{
	//Variables
	//Static variables
	protected static String fileName = "files/Problem42Words.txt";
	protected static ArrayList<String> fileWords = new ArrayList<>();
	//Instance variables
	protected ArrayList<String> triangleWords;

	//Functions
	//Constructor
	public Problem42(){
		super("Triangular number t(n) - (n * (n + 1)) / 2. For A = 1, B = 2, ... find the number of trinagular words in the file");
		triangleWords = new ArrayList<>();
	}
	//Operational function
	//Read words from file into array
	private void readFile(){
		if(!fileWords.isEmpty()){
			return;
		}
		File file = new File(fileName);
		if(file.exists()){
			try{
				List<String> lines = Files.readAllLines(file.toPath());
				for(String line : lines){
					String[] words = line.split(",");
					for(String word : words){
						fileWords.add(word.replace("\"", ""));
					}
				}
			}
			catch(IOException error){
				throw new InvalidParameterException("Error reading file", error);
			}
		}
		else{
			throw new InvalidParameterException("File not found");
		}
	}
	//Tet the sum of all letters in the string passed in
	private long getValueFromWord(String word){
		long sum = 0;
		for(char ch : word.toUpperCase().toCharArray()){
			long val = ch - 'A' + 1L;
			sum += val;
		}
		return sum;
	}
	//Check if the number passed in is a triangular number
	private boolean isTriangularNumber(long num){
		long counter = 1;
		long triangularNumber = 0;
		while(triangularNumber < num){
			triangularNumber += counter++;
			if(triangularNumber == num){
				return true;
			}
		}
		return false;
	}
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothign and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Read the file into the array
		readFile();
		//Run through all elements in the array checking for triangular words
		for(String word : fileWords){
			long value = getValueFromWord(word);
			if(isTriangularNumber(value)){
				triangleWords.add(word);
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		triangleWords = new ArrayList<>();
	}

	//Gets
	//Returns a string with the solution to the porblem
	public String getResult(){
		solvedCheck("results");
		return String.format("The number of triangular numbers is %d", getNumberTriangularNumbers());
	}
	//Returns the triangular words
	public List<String> getTriangularWords(){
		solvedCheck("triangular words");
		@SuppressWarnings("unchecked")
		ArrayList<String> clone = (ArrayList<String>)triangleWords.clone();
		return clone;
	}
	//Returns the number of triangular words
	public int getNumberTriangularNumbers(){
		solvedCheck("number of triangular words");
		return triangleWords.size();
	}
}

/* Results:
The number of triangular numbers is 162
It took an average of 166.881 microseconds to run this problem through 100 iterations
*/
