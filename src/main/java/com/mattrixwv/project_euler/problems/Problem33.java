//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem33.java
//Matthew Ellison
// Created: 02-05-21
//Modified: 06-27-23
/*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s
We shall consider fractions like, 30/50 = 3/5, to be trivial examples
There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator
If the product of these four fractions is given in its lowest common terms, find the value of the denominator
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.ArrayAlgorithms;
import com.mattrixwv.NumberAlgorithms;


public class Problem33 extends Problem{
	//Variables
	//Static variables
	private static final int MIN_NUMERATOR = 10;	//The lowest the numerator can be
	private static final int MAX_NUMERATOR = 98;	//The highest the numerator can be
	private static final int MIN_DENOMINATOR = 11;	//The lowest the denominator can be
	private static final int MAX_DENOMINATOR = 99;	//The highest the denominator can be
	//Instance variables
	protected ArrayList<Integer> numerators;	//Holds the numerators that were found
	protected ArrayList<Integer> denominators;	//Holds the denominators that were found
	protected int prodDenominator;	//Holds the answer to the question

	//Functions
	//Constructor
	public Problem33(){
		super("If the product of these four fractions is given in its lowest common terms, find the value of the denominator");
		prodDenominator = 1;
		numerators = new ArrayList<>();
		denominators = new ArrayList<>();
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Search every possible numerator/denominator pair
		for(int denominator = MIN_DENOMINATOR;denominator <= MAX_DENOMINATOR;++denominator){
			for(int numerator = MIN_NUMERATOR;(numerator < denominator) && (numerator <= MAX_NUMERATOR);++numerator){
				String denom = Integer.toString(denominator);
				String num = Integer.toString(numerator);
				int tempNum = 0;
				int tempDenom = 1;

				//Check that this isn't a trivial example
				if((num.charAt(1) == '0') && (denom.charAt(1) == '0')){
					continue;
				}
				//Remove the offending digits if they exist
				else if(num.charAt(0) == denom.charAt(0)){
					tempNum = num.charAt(1) - 48;
					tempDenom = denom.charAt(1) - 48;
				}
				else if(num.charAt(0) == denom.charAt(1)){
					tempNum = num.charAt(1) - 48;
					tempDenom = denom.charAt(0) - 48;
				}
				else if(num.charAt(1) == denom.charAt(0)){
					tempNum = num.charAt(0) - 48;
					tempDenom = denom.charAt(1) - 48;
				}
				else if(num.charAt(1) == denom.charAt(1)){
					tempNum = num.charAt(0) - 48;
					tempDenom = denom.charAt(0) - 48;
				}

				//Test if the new fraction is the same as the old one
				if(((double)tempNum / (double)tempDenom) == ((double)numerator / (double)denominator)){
					numerators.add(numerator);
					denominators.add(denominator);
				}
			}
		}

		//Get the product of the numbers
		int numProd = ArrayAlgorithms.getProd(numerators);
		int denomProd = ArrayAlgorithms.getProd(denominators);
		//Get the gcd to reduce to lowest terms
		int gcd = NumberAlgorithms.gcd(numProd, denomProd);
		//Save the denominator
		prodDenominator = denomProd / gcd;


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		numerators.clear();
		denominators.clear();
		prodDenominator = 1;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The denominator of the product is %d", prodDenominator);
	}
	//Returns the list of numerators
	public List<Integer> getNumerators(){
		solvedCheck("list of numerators");
		return numerators;
	}
	//Returns the list of denominators
	public List<Integer> getDenominators(){
		solvedCheck("list of denominators");
		return denominators;
	}
	//Returns the answer to the question
	public int getProdDenominator(){
		solvedCheck("denominator");
		return prodDenominator;
	}
}

/* Results:
The denominator of the product is 100
It took an average of 522.671 microseconds to run this problem through 100 iterations
*/
