//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem39.java
//Mattrixwv
// Created: 08-20-22
//Modified: 06-30-23
//If p is the perimeter of a right triangle for which value of p <= 1000 is the number of solutions for the sides {a, b, c} maximized
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.Triple;


public class Problem39 extends Problem{
	//Variables
	//Static variables
	protected static long maxPerimeter = 1000;
	//Instace variables
	protected List<Triple<Long, Long, Long>> longestSolutions;

	//Functions
	//Constructor
	public Problem39(){
		super(String.format("If p is the perimeter of a right triangle for which value of p <= %d is the number of solutions for the sides {a, b, c} maximized", maxPerimeter));
		longestSolutions = new ArrayList<>();
	}
	//Operational functions
	private boolean isRightTriangle(long a, long b, long c){
		return (c * c) == ((a * a) + (b * b));
	}
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Loop for perimeter <= 1000
		for(long perimeter = 1;perimeter <= maxPerimeter;++perimeter){
			ArrayList<Triple<Long, Long, Long>> solutions = new ArrayList<>();
			for(long a = 1;(a * 3) <= perimeter;++a){
				for(long b = a;(a + b + b) <= perimeter;++b){
					for(long c = b;(a + b + c) <= perimeter;++c){
						long sum = a + b + c;
						if((isRightTriangle(a, b, c)) && (sum == perimeter)){
							solutions.add(new Triple<>(a, b, c));
						}
					}
				}
			}
			if(solutions.size() >= longestSolutions.size()){
				longestSolutions = solutions;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		longestSolutions = new ArrayList<>();
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The perimeter with the largest number of solutions is %d", getLongestPerimeter());
	}
	//Returns the array of solutions
	public List<Triple<Long, Long, Long>> getLongestSolutions(){
		solvedCheck("array of solutions");
		return longestSolutions;
	}
	//Returns the perimeter of the solution
	public long getLongestPerimeter(){
		solvedCheck("perimeter");
		long perimeter = 0;
		if((longestSolutions != null) && (!longestSolutions.isEmpty())){
			Triple<Long, Long, Long> solution = longestSolutions.get(0);
			perimeter = solution.getA() + solution.getB() + solution.getC();
		}
		return perimeter;
	}
}

/* Results:
The perimeter with the largest number of solutions is 840
It took an average of 6.564 seconds to run this problem through 100 iterations
*/
