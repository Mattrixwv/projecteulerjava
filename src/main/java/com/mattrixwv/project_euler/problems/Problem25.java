//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem25.java
//Matthew Ellison
// Created: 03-25-19
//Modified: 06-27-23
//What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.NumberAlgorithms;

import java.math.BigInteger;


public class Problem25 extends Problem{
	//Variables
	//Static variables
	private static final int NUM_DIGITS = 1000;	//The number of digits to calculate up to
	//Instance variables
	protected BigInteger number;	//The current Fibonacci number
	protected BigInteger index;		//The index of the current Fibonacci number just calculated

	//Functions
	//Constructor
	public Problem25(){
		super(String.format("What is the index of the first term in the Fibonacci sequence to contain %d digits?", NUM_DIGITS));
		number = BigInteger.ZERO;
		index = BigInteger.TWO;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Move through all Fibonacci numbers until you reach the one with at least NUM_DIGITS digits
		while(number.toString().length() < NUM_DIGITS){
			index = index.add(BigInteger.ONE);	//Increase the index number. Doing this at the beginning keeps the index correct at the end of the loop
			number = NumberAlgorithms.getFib(index);	//Calculate the number
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		number = BigInteger.ZERO;
		index = BigInteger.TWO;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The first Fibonacci number with %d digits is %s%nIts index is %d", NUM_DIGITS, number.toString(), index);
	}
	//Returns the Fibonacci number asked for
	public BigInteger getNumber(){
		solvedCheck("fibonacci number");
		return number;
	}
	//Returns the Fibonacci number asked for as a string
	public String getNumberString(){
		solvedCheck("fibonacci number as a string");
		return number.toString(10);
	}
	//Returns the index of the requested Fibonacci number
	public BigInteger getIndex(){
		solvedCheck("index of the fibonacci number");
		return index;
	}
	//Returns the index of the requested Fibonacci number as a string
	public String getIndexString(){
		solvedCheck("index of the fibonacci number as a string");
		return index.toString(10);
	}
	//Returns the index of the requested Fibonacci number as a long
	public long getIndexInt(){
		solvedCheck("index of the fibonacci number as an int");
		return index.longValue();
	}
}


/* Results:
The first Fibonacci number with 1000 digits is 1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816
Its index is 4782
It took an average of 662.517 milliseconds to run this problem through 100 iterations
*/
