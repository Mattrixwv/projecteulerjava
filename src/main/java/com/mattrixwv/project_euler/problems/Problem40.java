//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem40.java
//Mattrixwv
// Created: 08-20-22
//Modified: 06-30-23
//An irrational decimal fraction is created by concatenating the positive integers. Find the value of the following expression: d1 * d10 * d100 * d1000 * d10000 * d100000 * d1000000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;

import com.mattrixwv.ArrayAlgorithms;


public class Problem40 extends Problem{
	//Variables
	//Static variables
	private static final long LARGEST_SUBSCRIPT = 1000000;
	//Instance variables
	protected StringBuilder irrationalDecimal;
	protected long product;

	//Functions
	//Constructor
	public Problem40(){
		super("An irrational decimal fraction is created by concatenating the positive integers. Find the value of the following expression: d1 * d10 * d100 * d1000 * d10000 * d100000 * d1000000");
		irrationalDecimal = new StringBuilder();
		product = 0;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Add numbers to the string until it is long enough
		for(Long num = 1L;irrationalDecimal.length() < LARGEST_SUBSCRIPT;++num){
			irrationalDecimal.append(num.toString());
		}
		//Get integers from the string, multiply them, and save the value
		ArrayList<Long> nums = new ArrayList<>();
		nums.add(Long.valueOf(Character.toString(irrationalDecimal.charAt(0))));
		nums.add(Long.valueOf(Character.toString(irrationalDecimal.charAt(9))));
		nums.add(Long.valueOf(Character.toString(irrationalDecimal.charAt(99))));
		nums.add(Long.valueOf(Character.toString(irrationalDecimal.charAt(999))));
		nums.add(Long.valueOf(Character.toString(irrationalDecimal.charAt(9999))));
		nums.add(Long.valueOf(Character.toString(irrationalDecimal.charAt(99999))));
		nums.add(Long.valueOf(Character.toString(irrationalDecimal.charAt(999999))));
		product = ArrayAlgorithms.getLongProd(nums);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		irrationalDecimal = new StringBuilder();
		product = 0;
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The product is %d", product);
	}
	//Returns the string of the decimal
	public String getIrrationalDecimal(){
		solvedCheck("decimal string");
		return "0." + irrationalDecimal.toString();
	}
	//Returns the product of the digits
	public long getProduct(){
		solvedCheck("product");
		return product;
	}
}

/* Results:
The product is 210
It took an average of 4.062 milliseconds to run this problem through 100 iterations
*/
