//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem44.java
//Mattrixwv
// Created: 08-20-22
//Modified: 06-30-23
//Pentagonal number P = ((n * (3 * n - 1)) / 2). Find 2 pentagonal numbers whos sum and difference are also pentagonal numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.generators.PentagonalNumberGenerator;


public class Problem44 extends Problem{
	//Variables
	//Instance variables
	protected PentagonalNumberGenerator generator;
	protected List<Long> pentagonalList;
	protected long pentagonalNumber1;
	protected long pentagonalNumber2;

	//Functions
	//Constructor
	public Problem44(){
		super("Find 2 pentagonal numbers whos sum and difference are also pentagonal numbers");
		generator = new PentagonalNumberGenerator();
		pentagonalList = new ArrayList<>();
		pentagonalNumber1 = 0;
		pentagonalNumber2 = 0;
	}
	//Operational functions
	//Solve the porblem
	public void solve(){
		//If the porblem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Keep generating pentagonal numbers and checking if their sum and difference is also pentagonal
		while(pentagonalNumber1 == 0){
			//Generate a new number and check it against every element we have already generated
			Long newNum = generator.next();
			//Go through every number in the list and check if their sum and difference are pentagonal
			for(Long num : pentagonalList){
				if(PentagonalNumberGenerator.isPentagonal(newNum + num) && PentagonalNumberGenerator.isPentagonal(newNum - num)){
					pentagonalNumber1 = num;
					pentagonalNumber2 = newNum;
					break;
				}
			}
			pentagonalList.add(newNum);
		}


		//Stop the timer
		timer.stop();

		//Set a flag to show the porblem is solved
		solved = true;
	}
	//Reset the porblem so it can be run again
	@Override
	public void reset(){
		super.reset();
		generator = new PentagonalNumberGenerator();
		pentagonalList = new ArrayList<>();
		pentagonalNumber1 = 0;
		pentagonalNumber2 = 0;
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The difference of the pentagonal numbers is %d", Math.abs(pentagonalNumber1 - pentagonalNumber2));
	}
	//Returns the list of pentagaonl numbers
	public List<Long> getPentagonalList(){
		solvedCheck("pentagonal list");
		return new ArrayList<>(pentagonalList);
	}
	//Returns the first pentagonal number
	public long getPentagonalNumber1(){
		solvedCheck("first pentagonal number");
		return pentagonalNumber1;
	}
	//Returns the second pentagonal number
	public long getPentagonalNumber2(){
		solvedCheck("second pentagonal number");
		return pentagonalNumber2;
	}
	//Returns the difference between the 2 pentagonal numbers
	public long getDifference(){
		solvedCheck("difference between the pentagonal numbers");
		return Math.abs(pentagonalNumber1 - pentagonalNumber2);
	}
}

/* Results:
The difference of the pentagonal numbers is 5482660
It took an average of 23.359 milliseconds to run this problem through 100 iterations
*/
