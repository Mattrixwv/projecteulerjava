//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem32.java
//Matthew Ellison
// Created: 07-27-20
//Modified: 06-27-23
//Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;

import com.mattrixwv.StringAlgorithms;


public class Problem32 extends Problem{
	//Structures
	//Holds the set of numbers that make a product
	protected class ProductSet{
		private int multiplicand;
		private int multiplier;
		public ProductSet(int multiplicand, int multiplier){
			this.multiplicand = multiplicand;
			this.multiplier = multiplier;
		}
		public int getMultiplicand(){
			return multiplicand;
		}
		public int getMultiplier(){
			return multiplier;
		}
		public int getProduct(){
			return (multiplicand * multiplier);
		}
		@Override
		public boolean equals(Object o){
			//If an object is compared to itself return true
			if(o == this){
				return true;
			}
			//Check that the object is the correct type
			if(!(o instanceof ProductSet)){
				return false;
			}
			ProductSet secondSet = (ProductSet)o;
			//Return true if the products are the same
			return (getProduct() == secondSet.getProduct());
		}
		@Override
		public int hashCode(){
			return (2 * multiplicand) + (3 * multiplier);
		}
		@Override
		public String toString(){
			return String.format("%d%d%d", multiplicand, multiplier, getProduct());
		}
	}

	//Variables
	//Static variables
	private static final int TOP_MULTIPLICAND = 99;	//The largest multiplicand to check
	private static final int TOP_MULTIPLIER = 4999;	//The largest multiplier to check
	//Instance variables
	protected ArrayList<ProductSet> listOfProducts;	//The list of unique products that are 1-9 pandigital
	protected long sumOfPandigitals;	//The sum of the products of the pandigital numbers

	//Functions
	//Constructor
	public Problem32(){
		super("Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.");
		listOfProducts = new ArrayList<>();
		sumOfPandigitals = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Create the multiplicand and start working your way up
		for(int multiplicand = 1;multiplicand <= TOP_MULTIPLICAND;++multiplicand){
			//Run through all possible multipliers
			for(int multiplier = multiplicand;multiplier <= TOP_MULTIPLIER;++multiplier){
				ProductSet currentProductSet = new ProductSet(multiplicand, multiplier);
				//If the product is too long move on to the next possible number
				if(currentProductSet.toString().length() > 9){
					break;
				}
				//If the current number is a pandigital that doesn't already exist in the list add it to the list
				if(isPandigital(currentProductSet) && !listOfProducts.contains(currentProductSet)){
					listOfProducts.add(currentProductSet);
				}
			}
		}

		//Get the sum of the products of the pandigitals
		for(ProductSet prod : listOfProducts){
			sumOfPandigitals += prod.getProduct();
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Returns true if the passed productset is 1-9 pandigital
	private boolean isPandigital(ProductSet currentSet){
		//Get the numbers out of the object and put them into a string
		String numberString = currentSet.toString();
		//Make sure the string is the correct length
		if(numberString.length() != 9){
			return false;
		}
		//Make sure every number from 1-9 is contained exactly once
		for(int panNumber = 1;panNumber <= 9;++panNumber){
			//Make sure there is exactly one of this number contained in the string
			final int tempNum = panNumber;	//This is here because forDigit() wanted a final variable
			if(StringAlgorithms.findNumOccurrence(numberString, Character.forDigit(tempNum, 10)) != 1){
				return false;
			}
		}
		//If all numbers were found in the string return true
		return true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		listOfProducts.clear();
		sumOfPandigitals = 0;
	}
	//Gets
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("There are %d unique 1-9 pandigitals%nThe sum of the products of these pandigitals is %d", listOfProducts.size(), sumOfPandigitals);
	}
	//Returns the sum of the pandigitals
	public long getSumOfPandigitals(){
		solvedCheck("sum of the pandigitals");
		return sumOfPandigitals;
	}
}


/* Results:
There are 7 unique 1-9 pandigitals
The sum of the products of these pandigitals is 45228
It took an average of 63.456 milliseconds to run this problem through 100 iterations
*/
