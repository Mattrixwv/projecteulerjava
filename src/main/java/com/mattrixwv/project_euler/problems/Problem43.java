//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem43.java
//Mattrixwv
// Created: 08-20-22
//Modified: 06-30-23
//Find the sum of all 0-9 pandigital numbers with this property
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.ArrayAlgorithms;
import com.mattrixwv.NumberAlgorithms;
import com.mattrixwv.StringAlgorithms;


public class Problem43 extends Problem{
	//Variables
	//Static variables
	protected static String nums = "0123456789";
	//Instance variables
	protected ArrayList<Long> subPrimePandigitals;

	//Functions
	//Constructor
	public Problem43(){
		super("Find the sum of all 0-9 pandigital numbers with this property");
		subPrimePandigitals = new ArrayList<>();
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the porblem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get all of the 0-9 pandigitals
		List<String> pandigitals = StringAlgorithms.getPermutations(nums);
		//Get all of the primes we need
		List<Long> primes = NumberAlgorithms.getNumPrimes(7L);
		//Break them into their component parts and see if they are divisible by the prime numbers
		for(String pandigital : pandigitals){
			boolean foundPrime = true;
			for(int cnt = 3;cnt < pandigital.length();++cnt){
				String subNum = pandigital.substring(cnt - 2, cnt + 1);
				long num = Long.parseLong(subNum);
				if((num % primes.get(cnt - 3)) != 0){
					foundPrime = false;
					break;
				}
			}
			if(foundPrime){
				Long num = Long.parseLong(pandigital);
				subPrimePandigitals.add(num);
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		subPrimePandigitals = new ArrayList<>();
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The sum of all pandigitals with the property is %d", getSumSubPrimePandigitals());
	}
	//Returns the list of sub-prime Pandigitals
	public List<Long> getSubPrimePandigitals(){
		solvedCheck("sub prime pandigitals");
		return subPrimePandigitals;
	}
	//Returns the sum of the list of sub-prime pandigitals
	public long getSumSubPrimePandigitals(){
		solvedCheck("sum of sub prime pandigitals");
		return ArrayAlgorithms.getLongSum(subPrimePandigitals);
	}
}

/* Results:
The sum of all pandigitals with the property is 16695334890
It took an average of 1.008 seconds to run this problem through 100 iterations
*/
