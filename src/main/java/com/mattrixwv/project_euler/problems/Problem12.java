//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem12.java
//Matthew Ellison
// Created: 03-04-19
//Modified: 06-27-23
//What is the value of the first triangle number to have over five hundred divisors?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.NumberAlgorithms;


public class Problem12 extends Problem{
	//Variables
	//Static variables
	private static final Long GOAL_DIVISORS = 500L;	//The minimum number of divisors that you want
	//Instance variables
	protected long sum;		//The sum of the numbers up to counter
	protected long counter;	//The next number to be added to sum
	protected List<Long> divisors;	//Holds the divisors of the triangular number sum

	//Functions
	//Constructor
	public Problem12(){
		super(String.format("What is the value of the first triangle number to have over %d divisors?", GOAL_DIVISORS));
		sum = 1;
		counter = 2;
		divisors = new ArrayList<>();
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Setup the other variables
		boolean foundNumber = false;	//To flag whether the number has been found

		//Start the timer
		timer.start();


		//Loop until you find the appropriate number
		while((!foundNumber) && (sum > 0)){
			divisors = NumberAlgorithms.getDivisors(sum);
			//If the number of divisors is correct set the flag
			if(divisors.size() > GOAL_DIVISORS.intValue()){
				foundNumber = true;
			}
			//Otherwise add to the sum and increase the next number
			else{
				sum += counter;
				++counter;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		sum = 1;
		counter = 2;
		divisors.clear();
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The triangular number %d is the sum of all numbers >= %d and has %d divisors", sum, counter - 1, divisors.size());
	}
	//Returns the triangular number
	public long getTriangularNumber(){
		solvedCheck("triangular number");
		return sum;
	}
	//Get the final number that was added to the triangular number
	public long getLastNumberAdded(){
		solvedCheck("last number added to get the triangular number");
		return counter - 1;
	}
	//Returns the list of divisors of the requested number
	public List<Long> getDivisorsOfTriangularNumber(){
		solvedCheck("divisors of the triangular number");
		return divisors;
	}
	//Returns the number of divisors of the requested number
	public int getNumberOfDivisors(){
		solvedCheck("number of divisors of the triangular number");
		return divisors.size();
	}
}


/* Results:
The triangular number 76576500 is the sum of all numbers >= 12375 and has 576 divisors
It took an average of 344.173 milliseconds to run this problem through 100 iterations
*/
