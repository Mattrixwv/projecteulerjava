//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem30.java
//Matthew Ellison
// Created: 10-27-19
//Modified: 06-27-23
//Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.ArrayAlgorithms;


public class Problem30 extends Problem{
	//Variables
	//Static variables
	private static final long TOP_NUM = 1000000;	//This is the largest number that will be checked
	private static final long BOTTOM_NUM = 2;		//Starts with 2 because 0 and 1 don't count
	private static final long POWER_RAISED = 5;		//This is the power that the digits are raised to
	//Instance variables
	protected List<Long> sumOfFifthNumbers;	//This is an ArrayList of the numbers that are the sum of the fifth power of their digits
	protected long sum;	//This is the sum of the sumOfFifthNumbers array

	//Functions
	//Operational functions
	public Problem30(){
		super("Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.");
		sumOfFifthNumbers = new ArrayList<>();
		sum = 0;
	}
	//Operational functions
	//Returns an ArrayList with the individual digits of the number passed to it
	private ArrayList<Long> getDigits(long num){
		ArrayList<Long> listOfDigits = new ArrayList<>();	//This ArrayList holds the individual digits of num
		//The easiest way to get the individual digits of a number is by converting it to a string
		String digits = Long.toString(num);
		//Start with the first digit, convert it to an integer, store it in the ArrayList, and move to the next digit
		for(int cnt = 0;cnt < digits.length();++cnt){
			listOfDigits.add(Long.valueOf(digits.substring(cnt, cnt + 1)));
		}
		//Return the list of digits
		return listOfDigits;
	}
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start with the lowest number and increment until you reach the largest number
		for(long currentNum = BOTTOM_NUM;currentNum <= TOP_NUM;++currentNum){
			//Get the digits of the number
			ArrayList<Long> digits = getDigits(currentNum);
			//Get the sum of the powers
			long sumOfPowers = 0L;
			for(long num : digits){
				sumOfPowers += Math.round(Math.pow(num, POWER_RAISED));
			}
			//Check if the sum of the powers is the same as the number
			//If it is add it to the list, otherwise continue to the next number
			if(sumOfPowers == currentNum){
				sumOfFifthNumbers.add(currentNum);
			}
		}

		//Get the sum of the numbers
		sum = ArrayAlgorithms.getLongSum(sumOfFifthNumbers);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		sumOfFifthNumbers.clear();
		sum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all the numbers that can be written as the sum of the fifth powers of their digits is %d", sum);
	}
	//This returns the top number to be checked
	public static long getTopNum(){
		return TOP_NUM;
	}
	//This returns a copy of the vector holding all the numbers that are the sum of the fifth power of their digits
	public List<Long> getListOfSumOfFifths(){
		solvedCheck("list of all numbers that are the sum of the 5th power of their digits");
		return sumOfFifthNumbers;
	}
	//This returns the sum of all entries in sumOfFifthNumbers
	public long getSumOfList(){
		solvedCheck("sum of all numbers that are the sum of the 5th power of their digits");
		return sum;
	}
}


/* Results:
The sum of all the numbers that can be written as the sum of the fifth powers of their digits is 443839
It took an average of 240.500 milliseconds to run this problem through 100 iterations
*/
