//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem18.java
//Matthew Ellison
// Created: 03-11-19
//Modified: 06-27-23
//Find the maximum total from top to bottom
//This is done using a breadth first search
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;


public class Problem18 extends Problem{
	//Structures
	//Used to keep track of where the best location came from
	protected static class Location{
		private int xLocation;
		private int yLocation;
		private int total;
		//Used originally for debugging so I could trace the path backwards
		private boolean fromRight;
		Location(int x, int y, int t, boolean r){
			xLocation = x;
			yLocation = y;
			total = t;
			fromRight = r;
		}

		public void setXLocation(int xLocation){ this.xLocation = xLocation; }
		public int getXLocation(){ return xLocation; }
		public void setYLocation(int yLocation){ this.yLocation = yLocation; }
		public int getYLocation(){ return yLocation; }
		public void setTotal(int total){ this.total = total; }
		public int getTotal(){ return total; }
		public void setFromRight(boolean fromRight){ this.fromRight = fromRight; }
		public boolean getFromRight(){ return fromRight; }
	}

	//Variables
	//Static variables
	protected String fileName = "files/Problem18Pyramid.txt";
	protected static ArrayList<ArrayList<Integer>> list;	//The list to hold the numbers in
	//Instance variables
	protected ArrayList<Location> foundPoints;	//For the points that you have already found the shortest distance to
	protected ArrayList<Location> possiblePoints;	//For the locations you are checking this round
	protected int actualTotal;	//The true total of the path from the top to the bottom

	//Functions
	//Constructor
	public Problem18(){
		super("Find the maximum total from top to bottom");
		foundPoints = new ArrayList<>();
		possiblePoints = new ArrayList<>();
		actualTotal = 0;
	}
	//Operational functions
	//Read number from file into number
	private void readFile(){
		if(list != null){
			return;
		}
		File file = new File(fileName);
		if(file.exists()){
			try{
				List<String> lines = Files.readAllLines(file.toPath());
				list = new ArrayList<>();
				for(String line : lines){
					ArrayList<Integer> working = new ArrayList<>();
					for(String num : line.split(" ")){
						working.add(Integer.parseInt(num));
					}
					list.add(working);
				}
			}
			catch(IOException error){
				throw new InvalidParameterException("Error reading file", error);
			}
		}
		else{
			throw new InvalidParameterException("Error opening file");
		}
	}
	//This function turns every number in the array into (100 - num) to allow you to find the largest numbers rather than the smallest
	protected void invert(ArrayList<ArrayList<Integer>> list){
		//Loop through every row in the list
		for(int rowCnt = 0;rowCnt < list.size();++rowCnt){
			//Loop through every column in the list
			for(int colCnt = 0;colCnt < list.get(rowCnt).size();++colCnt){
				//The current element gets the value of 100 - value
				list.get(rowCnt).set(colCnt, 100 - list.get(rowCnt).get(colCnt));
			}
		}
	}
	//This function helps by removing the element that is the same as the minimum location
	protected void removeHelper(ArrayList<Location> possiblePoints, final Location minLoc){
		possiblePoints.removeIf(loc -> ((loc.xLocation == minLoc.xLocation) && (loc.yLocation == minLoc.yLocation)));
	}
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Read the file into the list
		readFile();

		//Start the timer
		timer.start();


		//Invert the list
		invert(list);

		//Add the first row as a found point because you have to go through the top element
		foundPoints.add(new Location(0, 0, list.get(0).get(0), false));
		//Add the second row as possible points
		possiblePoints.add(new Location(0, 1, (list.get(0).get(0) + list.get(1).get(0)), true));
		possiblePoints.add(new Location(1, 1, (list.get(0).get(0) + list.get(1).get(1)), false));
		boolean foundBottom = false;	//Used when you find a point at the bottom of the list

		//Loop until you find the bottom of the list
		while(!foundBottom){
			//Check which possible point gives us the lowest number. If more than one has the same number simply keep the first one
			Location minLoc = possiblePoints.get(0);
			for(Location loc : possiblePoints){
				if(loc.total < minLoc.total){
					minLoc = loc;
				}
			}

			//Remove it from the list of possible points
			removeHelper(possiblePoints, minLoc);

			//Add that point to the list of found points
			foundPoints.add(minLoc);

			//Add to the list of possible points from the point we just found and
			//If you are at the bottom raise the flag to end the program
			int xLoc = minLoc.xLocation;
			int yLoc = minLoc.yLocation + 1;	//Add one because you will always be moving to the next row
			if(yLoc >= list.size()){
				foundBottom = true;
			}
			else{
				possiblePoints.add(new Location(xLoc, yLoc, minLoc.total + list.get(yLoc).get(xLoc), true));
				//Advance the x location to simulate going right
				++xLoc;
				//Check if x is out of bounds
				if(xLoc < list.get(yLoc).size()){
					possiblePoints.add(new Location(xLoc, yLoc, minLoc.total + list.get(yLoc).get(xLoc), false));
				}
			}
		}

		//Invert the list again so it is correct
		invert(list);

		//Save the results
		//Get the correct total which will be the inversion of the current one
		actualTotal = ((100 * list.size()) - foundPoints.get(foundPoints.size() - 1).total);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		foundPoints.clear();
		possiblePoints.clear();
		actualTotal = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The value of the longest path is %d", actualTotal);
	}
	//Returns the pyramid that was traversed as a string
	public String getPyramid(){
		solvedCheck("pyramid of numbers");
		StringJoiner results = new StringJoiner("\n");
		//Loop through all elements of the list and print them
		for(ArrayList<Integer> row : list){
			StringJoiner rowJoiner = new StringJoiner(" ");
			for(int column : row){
				rowJoiner.add(String.format("%2d", column));
			}
			results.add(rowJoiner.toString());
		}
		return results.toString();
	}
	//Returns the trail the algorithm took as a string
	public String getTrail(){
		solvedCheck("trail of the shortest path");

		StringJoiner results = new StringJoiner("->");
		//Save the trail the algorithm took
		ArrayList<Location> trail = new ArrayList<>();
		trail.add(foundPoints.get(foundPoints.size() - 1));
		boolean top = false;
		while(!top){
			boolean found = false;
			int loc = foundPoints.size() - 1;
			Location toAdd = null;
			while(!found){
				Iterator<Location> it = foundPoints.iterator();
				Location tempLoc = new Location(0, 0, 0, false);
				for(int cnt = 0;cnt < loc;++cnt){
					tempLoc = it.next();
				}
				if(trail.get(0).fromRight){
					if((tempLoc.xLocation == trail.get(0).xLocation) && (tempLoc.yLocation == (trail.get(0).yLocation - 1))){
						found = true;
						toAdd = tempLoc;
					}
					else{
						--loc;
					}
				}
				else{
					if((tempLoc.xLocation == (trail.get(0).xLocation - 1)) && (tempLoc.yLocation == (trail.get(0).yLocation - 1))){
						found = true;
						toAdd = tempLoc;
					}
					else{
						--loc;
					}
				}
			}

			trail.add(0, toAdd);

			if(trail.get(0).yLocation == 0){
				top = true;
			}
		}

		for(Location loc : trail){
			results.add(list.get(loc.yLocation).get(loc.xLocation).toString());
		}

		return results.toString();
	}
	//Returns the total that was asked for
	public int getTotal(){
		solvedCheck("total");
		return actualTotal;
	}
}


/* Results:
The value of the longest path is 1074
It took an average of 198.418 microseconds to run this problem through 100 iterations
*/
