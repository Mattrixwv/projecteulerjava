//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem28.java
//Matthew Ellison
// Created: 09-22-19
//Modified: 06-27-23
//What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;


public class Problem28 extends Problem{
	//Variables
	//Instance variables
	protected ArrayList<ArrayList<Integer>> grid;	//Holds the grid that we will be filling and searching
	protected int sumOfDiagonals;	//Holds the sum of the diagonals of the grid

	//Functions
	//Constructor
	public Problem28(){
		super("What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction?");
		sumOfDiagonals = 0;
	}
	//Operational functions
	//Sets up the grid
	private void setupGrid(){
		grid = new ArrayList<>();
		//Fill the grid with 0's
		for(int cnt = 0;cnt < 1001;++cnt){
			//Add a blank ArrayList
			grid.add(new ArrayList<>());
			for(int cnt2 = 0;cnt2 < 1001;++cnt2){
				grid.get(cnt).add(0);
			}
		}

		boolean finalLocation = false;	//A flag to indicate if the final location to be filled has been reached
		//Set the number that is going to be put at each location
		int currentNum = 1;
		//Start with the middle location and set it correctly and advance the tracker to the next number
		int xLocation = 500;
		int yLocation = 500;
		grid.get(yLocation).set(xLocation, currentNum++);
		//Move right the first time
		++xLocation;
		//Move in a circular pattern until you reach the final location
		while(!finalLocation){
			//Move down until you reach a blank location on the left
			while(!grid.get(yLocation).get(xLocation - 1).equals(0)){
				grid.get(yLocation).set(xLocation, currentNum++);
				++yLocation;
			}

			//Move left until you reach a blank location above
			while(!grid.get(yLocation - 1).get(xLocation).equals(0)){
				grid.get(yLocation).set(xLocation, currentNum++);
				--xLocation;
			}

			//Move up until you reach a blank location to the right
			while(!grid.get(yLocation).get(xLocation + 1).equals(0)){
				grid.get(yLocation).set(xLocation, currentNum++);
				--yLocation;
			}

			//Move right until you reach a blank location below
			while(!grid.get(yLocation + 1).get(xLocation).equals(0)){
				grid.get(yLocation).set(xLocation, currentNum++);
				++xLocation;
				//Check if you are at the final location and break the loop if you are
				if(xLocation == grid.size()){
					finalLocation = true;
					break;
				}
			}
		}
	}
	//Finds the sum of the diagonals in the grid
	private void findSum(){
		//Start at the top corners and work your way down moving toward the opposite side
		int leftSide = 0;
		int rightSide = grid.size() - 1;
		int row = 0;
		while(row < grid.size()){
			//This ensures the middle location is only counted once
			if(leftSide == rightSide){
				sumOfDiagonals += grid.get(row).get(leftSide);
			}
			else{
				sumOfDiagonals += grid.get(row).get(leftSide);
				sumOfDiagonals += grid.get(row).get(rightSide);
			}
			++row;
			++leftSide;
			--rightSide;
		}
	}
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Setup the grid
		setupGrid();
		//Find the sum of the diagonals in the grid
		findSum();


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		sumOfDiagonals = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of the diagonals in the given grid is %d", sumOfDiagonals);
	}
	//Returns the grid
	public List<ArrayList<Integer>> getGrid(){
		solvedCheck("grid");
		return grid;
	}
	//Returns the sum of the diagonals
	public int getSum(){
		solvedCheck("sum");
		return sumOfDiagonals;
	}

}


/* Results:
The sum of the diagonals in the given grid is 669171001
It took an average of 17.307 milliseconds to run this problem through 100 iterations
*/
