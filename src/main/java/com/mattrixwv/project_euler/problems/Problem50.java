//ProjectEulerJava/src/main/java/com/mattrixwv/project_euler/problems/Problem50.java
//Mattrixwv
// Created: 08-23-22
//Modified: 06-30-23
//Which prime, below one-million, can be written as the sum of the most consecutive primes?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.ArrayAlgorithms;
import com.mattrixwv.NumberAlgorithms;


public class Problem50 extends Problem{
	//Variables
	//Static variables
	private static final long MAX = 999999;
	//Instance variables
	protected ArrayList<Long> consecutivePrimes;

	//Functions
	//Constructor
	public Problem50(){
		super(String.format("Which prime, below %d, can be written as the sum of the most consecutive primes?", MAX + 1));
		consecutivePrimes = new ArrayList<>();
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the porblem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get all primes < MAX
		List<Long> primes = NumberAlgorithms.getPrimes(MAX);
		//Check every length of consecutive primes for a prime sum. Stop when the length becomes too long
		boolean tooLong = false;
		for(long length = 1;!tooLong;++length){
			//If the length is even you only need to check offset 0 because of 2
			if((length % 2) == 0){
				checkEven(primes, length);
			}
			//If the length is odd you need to check every offset until the sum becomes too large
			else{
				tooLong = checkOdd(primes, length);
			}
		}


		//Stop the timer
		timer.stop();

		//Set a flag to show the problem is solved
		solved = true;
	}
	public void checkEven(List<Long> primes, long length){
		long sum = 0;
		//Get the sum of consecutive primes
		for(int cnt = 0;cnt < length;++cnt){
			sum += primes.get(cnt);
		}
		//If the new sum is prime save it
		if(primes.contains(sum)){
			consecutivePrimes = new ArrayList<>();
			for(int cnt = 0;cnt < length;++cnt){
				consecutivePrimes.add(primes.get(cnt));
			}
		}
	}
	public boolean checkOdd(List<Long> primes, long length){
		boolean tooLong = false;
		//Set the offset (always skipping 2 because this is an odd length sum)
		for(int start = 1;(start + length) < primes.size();++start){
			long sum = 0;
			//Get the sum of consecutive primes
			for(int cnt = 0;cnt < length;++cnt){
				sum += primes.get(start + cnt);
			}
			//If the sum is too large the offset has gotten too high, so break the loop
			if(sum > MAX){
				//If the offset is minimum break the length loop because all subsequent sums will be too large
				if(start == 1){
					tooLong = true;
				}
				break;
			}
			//If the new sum is prime save it
			else if(primes.contains(sum)){
				consecutivePrimes = new ArrayList<>();
				for(int cnt = 0;cnt < length;++cnt){
					consecutivePrimes.add(primes.get(start + cnt));
				}
				break;
			}
		}
		return tooLong;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		consecutivePrimes = new ArrayList<>();
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The prime below %d that can be written as the sum of the most consecutive primes is %d", MAX + 1, ArrayAlgorithms.getLongSum(consecutivePrimes));
	}
	//Returns the list of consecutive primes
	public List<Long> getConsecutivePrimes(){
		solvedCheck("list of consecutive primes");
		@SuppressWarnings("unchecked")
		ArrayList<Long> clone = (ArrayList<Long>)consecutivePrimes.clone();
		return clone;
	}
	//Returns the sum of the primes
	public long getPrime(){
		solvedCheck("prime");
		return ArrayAlgorithms.getLongSum(consecutivePrimes);
	}
}

/* Results:
The prime below 1000000 that can be written as the sum of the most consecutive primes is 997651
It took an average of 77.425 milliseconds to run this problem through 100 iterations
*/
