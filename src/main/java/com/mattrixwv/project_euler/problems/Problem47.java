//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem47.java
//Mattrixwv
// Created: 08-22-22
//Modified: 06-30-23
//What is the first of four consecutive integers to have four distinct prime factors each?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.HashMap;
import java.util.List;

import com.mattrixwv.NumberAlgorithms;


public class Problem47 extends Problem{
	//Variables
	//Instance variables
	protected long num;	//The first of the consecutive numbers

	//Function
	//Constructor
	public Problem47(){
		super("What is the first of four consecutive integers to have four distinct prime factors each?");
		num = 0;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Go through every number
		HashMap<Long, List<Long>> factors = new HashMap<>();
		factors.put(1L, NumberAlgorithms.getFactors(1L));
		factors.put(2L, NumberAlgorithms.getFactors(2L));
		factors.put(3L, NumberAlgorithms.getFactors(3L));
		for(long cnt = 1;num == 0;++cnt){
			//Get the next set of factors
			factors.put(cnt + 3, NumberAlgorithms.getFactors(cnt + 3));

			//Make maps of the factors and their powers
			HashMap<Long, Long> factors0 = new HashMap<>();
			for(Long factor : factors.get(cnt)){
				factors0.merge(factor, 1L, Long::sum);
			}
			HashMap<Long, Long> factors1 = new HashMap<>();
			for(Long factor : factors.get(cnt + 1)){
				factors1.merge(factor, 1L, Long::sum);
			}
			HashMap<Long, Long> factors2 = new HashMap<>();
			for(Long factor : factors.get(cnt + 2)){
				factors2.merge(factor, 1L, Long::sum);
			}
			HashMap<Long, Long> factors3 = new HashMap<>();
			for(Long factor : factors.get(cnt + 3)){
				factors3.merge(factor, 1L, Long::sum);
			}

			//Make sure all of the maps have the correct number of elements
			if((factors0.keySet().size() != 4) || (factors1.keySet().size() != 4) || (factors2.keySet().size() != 4) || (factors3.keySet().size() != 4)){
				continue;
			}

			num = cnt;
		}


		//Stop the timer
		timer.stop();

		//Set a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		num = 0;
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The first number is %d", num);
	}
	//Returns the number
	public long getFirstNum(){
		solvedCheck("first number");
		return num;
	}
}

/* Results
The first number is 134043
It took 51.366 seconds to solve this problem.
*/
