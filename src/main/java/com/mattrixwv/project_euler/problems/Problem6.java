//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem6.java
//Matthew Ellison
// Created: 03-01-19
//Modified: 06-27-23
//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


public class Problem6 extends Problem{
	//Variables
	//Static variables
	private static final int START_NUM = 1;		//The first number that needs to be counted
	private static final int END_NUM = 100;		//The last number that needs to be counted
	//Instance variables
	protected long sumOfSquares;	//Holds the sum of the squares of all the numbers
	protected long squareOfSum;		//Holds the square of the sum of all the numbers

	//Functions
	//Constructor
	public Problem6(){
		super(String.format("Find the difference between the sum of the squares and the square of the sum of the numbers %d-%d.", START_NUM, END_NUM));
		sumOfSquares = 0;
		squareOfSum = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Run through all numbers and add them to the appropriate sums
		for(int currentNum = START_NUM;currentNum <= END_NUM;++currentNum){
			sumOfSquares += (currentNum * currentNum);	//Add the square to the correct variable
			squareOfSum += currentNum;	//Add the number to the correct variable to squaring later
		}
		//Squaring the sum that needs it
		squareOfSum *= squareOfSum;


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		squareOfSum = 0;
		sumOfSquares = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is %d", Math.abs(sumOfSquares - squareOfSum));
	}
	//Returns the sum of all the squares
	public long getSumOfSquares(){
		solvedCheck("sum of the squares");
		return sumOfSquares;
	}
	//Returns the square of all of the sums
	public long getSquareOfSum(){
		solvedCheck("square of the sums");
		return squareOfSum;
	}
	//Returns the requested difference
	public long getDifference(){
		solvedCheck("difference between the two numbers");
		return Math.abs(sumOfSquares - squareOfSum);
	}
}


/* Results:
The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is 25164150
It took an average of 2.669 microseconds to run this problem through 100 iterations
*/
