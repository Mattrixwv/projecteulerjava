//ProjectEulerJava/src/main/java/mattrixwv/project_euler/Problems/Problem46.java
//Mattrixwv
// Created: 08-22-22
//Modified: 06-30-23
//What is the smallest odd coposite number that cannot be written as the sum of a prime and twice a square?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;

import com.mattrixwv.generators.SieveOfEratosthenes;


public class Problem46 extends Problem{
	//Variables
	//Instance variables
	protected long num;

	//Functions
	//Constructor
	public Problem46(){
		super("What is the smallest odd composite number that connot be written as the sum of a prime and twice a square?");
		num = 0;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Setup a prime sieve
		SieveOfEratosthenes sieve = new SieveOfEratosthenes();
		ArrayList<Long> primes = new ArrayList<>();
		primes.add(sieve.next());
		//Run until we find a composite number
		for(int currentOdd = 3;num == 0;currentOdd += 2){
			boolean foundEquation = false;
			//Add elements to the prime array until you reach a number greater than the current number to test
			while(primes.get(primes.size() - 1) < currentOdd){
				primes.add(sieve.next());
			}
			//Make sure the number isn't prime
			if(primes.get(primes.size() - 1) == currentOdd){
				continue;
			}
			//Check if the number can be calculated with the formula
			//Start with the smallest prime and work your way up
			for(long prime : primes){
				//Start with 1 and work your way up until the number becomes too large
				boolean tooLarge = false;
				for(int cnt = 1;(!tooLarge) && (!foundEquation);++cnt){
					//Find the value of the equation for these numbers
					long equation = prime + (2 * cnt * cnt);
					//If the number is too large break the loop
					if(equation > currentOdd){
						tooLarge = true;
					}
					//If the number == equation set a flag to break the loops
					else if(equation == currentOdd){
						foundEquation = true;
					}
				}
			}
			if(!foundEquation){
				num = currentOdd;
			}
		}


		//Stop the timer
		timer.stop();

		//Set a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		num = 0;
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The smallest odd composite that cannot be written as the sum of a prime and twice a square is %d", num);
	}
	//Returns the number
	public long getCompositeNum(){
		solvedCheck("composite number");
		return num;
	}
}

/* Results:
The smallest odd composite that cannot be written as the sum of a prime and twice a square is 5777
It took an average of 5.603 milliseconds to run this problem through 100 iterations
*/
