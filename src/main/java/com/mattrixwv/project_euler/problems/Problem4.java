//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem4.java
//Matthew Ellison
// Created: 03-01-19
//Modified: 06-27-23
//Find the largest palindrome made from the product of two 3-digit numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Problem4 extends Problem{
	//Variables
	//Static variables
	private static final int START_NUM = 100;	//The first number to be multiplied
	private static final int END_NUM = 999;		//The last number to be multiplied
	//Instance variables
	protected List<Integer> palindromes;	//Holds all numbers that turn out to be palindromes

	//Constructor
	public Problem4(){
		super("Find the largest palindrome made from the product of two 3-digit numbers");
		palindromes = new ArrayList<>();
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start at the first 3-digit number and check every one up to the last 3-digit number
		for(int firstNum = START_NUM;firstNum <= END_NUM;++firstNum){
			//You can start at the location of the first number because everything before that has already been tested. (100 * 101 == 101 * 100)
			for(int secondNum = firstNum;secondNum < END_NUM;++secondNum){
				//Get the product
				int product = firstNum * secondNum;
				//Change the number into a string
				String productString = Integer.toString(product);
				//Reverse the string
				String reverseString = new StringBuilder(productString).reverse().toString();
				
				//If the number and it's reverse are the same it is a palindrome so add it to the list
				if(productString.equals(reverseString)){
					palindromes.add(product);
				}
				//If it's not a palindrome ignore it and move to the next number
			}
		}

		//Sort the palindromes so that the last one is the largest
		Collections.sort(palindromes);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		palindromes.clear();
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The largest palindrome is %d", palindromes.get(palindromes.size() - 1));
	}
	//Returns the list of all palindromes
	public List<Integer> getPalindromes(){
		solvedCheck("palindromes");
		return palindromes;
	}
	//Returns the largest palindrome
	public int getLargestPalindrome(){
		solvedCheck("largest palindrome");
		return palindromes.get(palindromes.size() - 1);
	}
}


/* Results:
The largest palindrome is 906609
It took an average of 16.926 milliseconds to run this problem through 100 iterations
*/
