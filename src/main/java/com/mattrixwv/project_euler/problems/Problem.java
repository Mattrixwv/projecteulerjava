//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem.java
//Matthew Ellison
// Created: 03-01-19
//Modified: 08-20-22
//This is a base class for problems to use as a template
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import com.mattrixwv.Stopwatch;
import com.mattrixwv.exceptions.InvalidResult;
import com.mattrixwv.project_euler.exceptions.Unsolved;


public abstract class Problem{
	//Variables
	//Instance variables
	protected Stopwatch timer = new Stopwatch();	//To time how long it takes to run the algorithm
	private final String description;	//Holds the description of the problem
	protected boolean solved;	//Shows whether the problem has already been solved

	//Constructor
	protected Problem(String description){
		this.description = description;
		solved = false;
	}

	//Gets
	//Returns the description of the problem
	public String getDescription(){
		return description;
	}
	//Returns the result of solving the problem
	public abstract String getResult();
	//Returns the time taken to run the problem as a string
	public String getTime() throws InvalidResult, Unsolved{
		if(!solved){
			throw new Unsolved();
		}
		return timer.getStr();
	}
	//Returns the timer as a stopwatch
	public Stopwatch getTimer(){
		if(!solved){
			throw new Unsolved();
		}
		return timer;
	}
	protected void solvedCheck(String str){
		if(!solved){
			throw new Unsolved("You must solve the problem before you can see the " + str);
		}
	}
	public boolean getSolved(){
		return solved;
	}
	//Solve the problem
	public abstract void solve() throws InvalidResult;
	//Reset the problem so it can be run again
	public void reset(){
		timer.reset();
		solved = false;
	}
}
