//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem26.java
//Matthew Ellison
// Created: 07-28-19
//Modified: 06-27-23
//Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.util.ArrayList;


public class Problem26 extends Problem{
	//Variables
	//Static variables
	private static final int TOP_NUM = 999;	//The largest denominator to test
	//Instance variables
	protected int longestCycle;		//The length of the longest cycle
	protected int longestNumber;	//The starting denominator of the longest cycle


	//Functions
	//Constructor
	public Problem26(){
		super(String.format("Find the value of d <= %d for which 1/d contains the longest recurring cycle in its decimal fraction part.", TOP_NUM));
		longestCycle = 0;
		longestNumber = 1;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start with 1/2 and find out how long the longest cycle is by checking the remainders
		//Loop through every number from 2-999 and use it for the denominator
		for(int denominator = 2;denominator <= TOP_NUM;++denominator){
			ArrayList<Integer> denomList = new ArrayList<>();
			boolean endFound = false;	//A flag for when we have found an end to the number (either a cycle or a 0 for remainder)
			boolean cycleFound = false;	//A flag to indicate a cycle was detected
			int numerator = 1;	//The numerator that will be divided. Always starts at 1
			while(!endFound){
				//Get the remainder after the division
				int remainder = numerator % denominator;
				//Check if the remainder is 0 and set the flag
				if(remainder == 0){
					endFound = true;
				}
				//Check if the remainder is in the list and set the appropriate flags
				else if(denomList.contains(remainder)){
					endFound = true;
					cycleFound = true;
				}
				//Else add it to the list
				else{
					denomList.add(remainder);
				}
				//Multiply the remainder by 10 to continue finding the next remainder
				numerator = remainder * 10;
			}
			//If a cycle was found check the size of the list against the largest cycle
			//If it is larger than the largest, set it as the new largest
			if(cycleFound && (denomList.size() > longestCycle)){
				longestCycle = denomList.size();
				longestNumber = denominator;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		longestCycle = 0;
		longestNumber = 1;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The longest cycle is %d digits long%nIt started with the number %d", longestCycle, longestNumber);
	}
	//Returns the length of the longest cycle
	public int getLongestCycle(){
		solvedCheck("length of the longest cycle");
		return longestCycle;
	}
	//Returns the denominator that starts the longest cycle
	public int getLongestNumber(){
		solvedCheck("denominator that starts the longest cycle");
		return longestNumber;
	}
}


/* Results:
The longest cycle is 982 digits long
It started with the number 983
It took an average of 12.182 milliseconds to run this problem through 100 iterations
*/
