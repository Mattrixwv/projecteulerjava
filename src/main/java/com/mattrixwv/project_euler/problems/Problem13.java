//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem13.java
//Matthew Ellison
// Created: 03-04-19
//Modified: 06-27-23
//Work out the first ten digits of the sum of the following one-hundred 50-digit numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import com.mattrixwv.ArrayAlgorithms;


public class Problem13 extends Problem{
	//Variables
	//Static variables
	protected static String fileName = "files/Problem13Numbers.txt";
	protected static ArrayList<BigInteger> nums;	//Holds the numbers that are being summed
	//Instance variables
	protected BigInteger sum;	//The sum of all the numbers

	//Functions
	//Constructor
	public Problem13(){
		super("Work out the first ten digits of the sum of the one-hundred 50-digit numbers");
		sum = BigInteger.ZERO;
	}
	//Operational functions
	//Read number from file into nums
	private static void readFile(){
		if(nums != null){
			return;
		}
		File file = new File(fileName);
		if(file.exists()){
			try{
				List<String> lines = Files.readAllLines(file.toPath());
				nums = new ArrayList<>();
				for(String line : lines){
					nums.add(new BigInteger(line));
				}
			}
			catch(IOException error){
				throw new InvalidParameterException("Error reading file", error);
			}
		}
		else{
			throw new InvalidParameterException("Error opening file");
		}
	}
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Read the file into the number
		readFile();

		//Start the timer
		timer.start();


		//Get the sum of all the numbers
		sum = ArrayAlgorithms.getBigSum(nums);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		sum = BigInteger.ZERO;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all %d numbers is %d%nThe first 10 digits of the sum of the numbers is %s", nums.size(), sum, (sum.toString()).substring(0, 10));
	}
	//Returns the list of 50-digit numbers
	public List<BigInteger> getNumbers(){
		solvedCheck("numbers");
		return nums;
	}
	//Returns the sum of the 50-digit numbers
	public BigInteger getSum(){
		solvedCheck("sum");
		return sum;
	}
}


/* Results:
The sum of all 100 numbers is 5537376230390876637302048746832985971773659831892672
The first 10 digits of the sum of the numbers is 5537376230
It took an average of 129.354 microseconds to run this problem through 100 iterations
*/
