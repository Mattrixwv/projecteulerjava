//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem48.java
//Mattrixwv
// Created: 08-23-22
//Modified: 06-30-23
//Find the last ten digits of the series 1^1 + 2^2 + 3^3 + ... + 1000^1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


import java.math.BigInteger;


public class Problem48 extends Problem{
	//Variables
	//Static variables
	private static final BigInteger TOP_NUM = BigInteger.valueOf(1000);
	//Instance variables
	protected BigInteger sum;

	//Functions
	//Constructor
	public Problem48(){
		super("Find the last ten digits of the series Σn^n for 1<=n<=1000");
		sum = BigInteger.ZERO;
	}
	//Operational functions
	//Solve the problem
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Start with 1 and execute the formula for each number
		for(BigInteger cnt = BigInteger.ONE;cnt.compareTo(TOP_NUM) <= 0;cnt = cnt.add(BigInteger.ONE)){
			sum = sum.add(cnt.pow(cnt.intValue()));
		}


		//Stop the timer
		timer.stop();

		//Set a flag to show the problem is solved
		solved = true;
	}
	//Reset the rpobelm so it can be run again
	@Override
	public void reset(){
		super.reset();
		sum = BigInteger.ZERO;
	}

	//Gets
	//Returns a string with the solution to the problem
	public String getResult(){
		solvedCheck("results");
		return String.format("The last ten digits of the series Σn^n for 1<=n<=1000 is %s", getSumDigits());
	}
	//Returns the sum of the series
	public BigInteger getSum(){
		solvedCheck("sum");
		return sum;
	}
	//Returns the last 10 digits of the sum
	public String getSumDigits(){
		solvedCheck("last 10 digits of the sum");
		String sumString = sum.toString();
		return sumString.substring(sumString.length() - 10);
	}
}

/* Results
The last ten digits of the series Σn^n for 1<=n<=1000 is 9110846700
It took an average of 3.831 milliseconds to run this problem through 100 iterations
*/
