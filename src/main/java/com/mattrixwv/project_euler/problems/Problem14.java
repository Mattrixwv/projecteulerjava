//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem14.java
//Matthew Ellison
// Created: 03-04-19
//Modified: 06-27-23
/*
The following iterative sequence is defined for the set of positive integers:
n → n/2 (n is even)
n → 3n + 1 (n is odd)
Which starting number, under one million, produces the longest chain?
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


public class Problem14 extends Problem{
	//Variables
	//Static variables
	private static final long MAX_NUM = 1000000L - 1L;	//This is the top number that you will be checking against the series
	//Instance variables
	protected long maxLength;	//This is the length of the longest chain
	protected long maxNum;		//This is the starting number of the longest chain

	//Functions
	//Constructor
	public Problem14(){
		super(String.format("Which starting number, under %d, produces the longest chain using the iterative sequence?", MAX_NUM + 1));
		maxLength = 0;
		maxNum = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Loop through all numbers <= MAX_NUM and check them against the series
		for(long currentNum = 1L;currentNum <= MAX_NUM;++currentNum){
			long currentLength = checkSeries(currentNum);
			//If the current number has a longer series than the max then the current becomes the max
			if(currentLength > maxLength){
				maxLength = currentLength;
				maxNum = currentNum;
			}
		}


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//This function follows the rules of the sequence and returns its length
	private long checkSeries(long num){
		long length = 1L;	//Start at 1 because you need to count the starting number

		//Follow the series, adding 1 for each step you take
		while(num > 1){
			if((num % 2) == 0){
				num /= 2;
			}
			else{
				num = (3 * num) + 1;
			}
			++length;
		}

		//Return the length of the series
		return length;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		maxLength = 0;
		maxNum = 0;
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The number %d produced a chain of %d steps", maxNum, maxLength);
	}
	//Returns the length of the requested chain
	public long getLength(){
		solvedCheck("length of the longest chain");
		return maxLength;
	}
	//Returns the starting number of the requested chain
	public long getStartingNumber(){
		solvedCheck("starting number of the longest chain");
		return maxNum;
	}
}


/* Results:
The number 837799 produced a chain of 525 steps
It took an average of 286.004 milliseconds to run this problem through 100 iterations
*/
