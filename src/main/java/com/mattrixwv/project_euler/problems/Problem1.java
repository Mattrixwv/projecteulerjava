//ProjectEulerJava/src/main/java/mattrixwv/ProjectEuler/Problems/Problem1.java
//Matthew Ellison
// Created: 03-01-19
//Modified: 06-27-23
//What is the sum of all the multiples of 3 or 5 that are less than 1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/JavaClasses
/*
	Copyright (C) 2023  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler.problems;


public class Problem1 extends Problem{
	//Variables
	//Static variables
	private static final int TOP_NUM = 999;	//The largest number to be checked
	//Instance variables
	protected int fullSum;	//The sum of all the numbers

	//Functions
	//Constructor
	public Problem1(){
		super(String.format("What is the sum of all the multiples of 3 or 5 that are less than %d?", (TOP_NUM + 1)));
		fullSum = 0;
	}
	//Operational functions
	//Solve the problem
	@Override
	public void solve(){
		//If the problem has already been solved do nothing and end the function
		if(solved){
			return;
		}

		//Start the timer
		timer.start();


		//Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
		fullSum = sumOfProgression(3) + sumOfProgression(5) - sumOfProgression(3 * 5.0);


		//Stop the timer
		timer.stop();

		//Throw a flag to show the problem is solved
		solved = true;
	}
	//Reset the problem so it can be run again
	@Override
	public void reset(){
		super.reset();
		fullSum = 0;
	}
	//Gets the sum of the progression of the multiple
	private int sumOfProgression(double multiple){
		double numTerms = Math.floor(TOP_NUM / multiple);	//This gets the number of multiples of a particular number that is < MAX_NUMBER
		//The sum of progression formula is (n / 2)(a + l). n = number of terms, a = multiple, l = last term
		return (int)((numTerms / 2.0) * (multiple + (numTerms * multiple)));
	}
	//Gets
	//Returns the result of solving the problem
	@Override
	public String getResult(){
		solvedCheck("result");
		return String.format("The sum of all numbers < %d is %d", (TOP_NUM + 1), fullSum);
	}
	//Returns the requested sum
	public int getSum(){
		solvedCheck("sum");
		return fullSum;
	}
}


/* Results:
The sum of all numbers < 1000 is 233168
It took an average of 298.000 nanoseconds to run this problem through 100 iterations
*/
