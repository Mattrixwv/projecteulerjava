//ProjectEuler/ProjectEulerJava/src/man/java/mattrixwv/ProjectEuler/Benchmark.java
//Matthew Ellison
// Created: 07-08-20
//Modified: 07-09-20
//This runs the benchmark functions for the Java version of the ProjectEuler project
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.project_euler;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import com.mattrixwv.Stopwatch;
import com.mattrixwv.exceptions.InvalidResult;

import com.mattrixwv.project_euler.problems.Problem;


public class Benchmark{
	private Benchmark(){}
	private static final Scanner input = new Scanner(System.in);
	private enum BenchmarkOptions{RUN_SPECIFIC, RUN_ALL_SHORT, RUN_ALL, EXIT, SIZE}
	private static final ArrayList<Integer> tooLong = new ArrayList<>(Arrays.asList(15, 23, 24, 35));
	//The driver function for the benchmark selection
	public static void benchmarkMenu() throws InvalidResult{
		BenchmarkOptions selection;

		printMenu();
		selection = getMenuSelection();

		switch(selection){
			case RUN_SPECIFIC : runSpecific(); break;
			case RUN_ALL_SHORT : runAllShort(); break;
			case RUN_ALL : runAll(); break;
			case EXIT : break;
			case SIZE : break;
		}
	}
	//Print the benchmark menu
	private static void printMenu(){
		System.out.println("1. Run a specific problem");
		System.out.println("2. Run all problems that have a reasonably short run time");
		System.out.println("3. Run all problems");
		System.out.println("4. Exit the menu");
		System.out.println();
	}
	//Returns a valid menu option
	private static BenchmarkOptions getMenuSelection(){
		int selection = input.nextInt();
		while(!isValidMenu(selection)){
			System.out.println("That is an invalid option!\nPress Enter to continue");
			printMenu();
			selection = input.nextInt();
		}
		return getSelection(selection);
	}
	//Determines if a value is a valid menu option. Helper for getBechmarkMenuSelection
	private static boolean isValidMenu(int selection){
		//Ordinal + 1 because enum starts at 0
		return (selection > 0) && (selection < (BenchmarkOptions.SIZE.ordinal() + 1));
	}
	//A helper function for getMenuSelection that turns an integer to a BenchmarkOptions
	private static BenchmarkOptions getSelection(Integer selection){
		BenchmarkOptions sel = null;

		switch(selection){
			case 1 : sel = BenchmarkOptions.RUN_SPECIFIC; break;
			case 2 : sel =  BenchmarkOptions.RUN_ALL_SHORT; break;
			case 3 : sel = BenchmarkOptions.RUN_ALL; break;
			case 4 : sel = BenchmarkOptions.EXIT; break;
			default : sel = BenchmarkOptions.SIZE;
		}
		return sel;
	}
	//Determines which problem user wants to run and runs it
	private static void runSpecific() throws InvalidResult{
		//Ask which problem the user wants to run
		int problemNumber = ProblemSelection.getProblemNumber();
		//Ask how many times to run the problem
		int timesToRun = getNumberOfTimesToRun();

		//Get the problem and print its description
		Problem problem = ProblemSelection.getProblem(problemNumber);
		System.out.println(problemNumber + ". " + problem.getDescription());

		//Run the problem the specific number of times
		double totalTime = runProblem(problem, timesToRun);

		//Print the results
		System.out.println(getBenchmarkResults(problem, totalTime, timesToRun));
	}
	//Runs all problems except a few that are specified because of run length
	private static void runAllShort() throws InvalidResult{
		//Ask how many times to run the problems
		int timesToRun = getNumberOfTimesToRun();

		//Run through all valid problem numbers, skipping a few that are in the tooLong list
		for(int cnt = 1;cnt < ProblemSelection.PROBLEM_NUMBERS.size();++cnt){
			int problemNumber = ProblemSelection.PROBLEM_NUMBERS.get(cnt);

			//If the problem number is contained in the list of problems that take too long skip it
			if(tooLong.contains(problemNumber)){
				continue;
			}

			//Get the problem and print its description
			Problem problem = ProblemSelection.getProblem(problemNumber);
			System.out.println(problemNumber + ". " + problem.getDescription());

			//Run each problem the specified number of times
			double totalTime = runProblem(problem, timesToRun);

			//Print the results
			System.out.println(getBenchmarkResults(problem, totalTime, timesToRun));
		}
	}
	//Runs all problems
	private static void runAll() throws InvalidResult{
		//Ask how many times to run the problem
		int timesToRun = getNumberOfTimesToRun();

		//Run through all valid problem numbers, skipping a few that are in the tooLong list
		for(int cnt = 1;cnt < ProblemSelection.PROBLEM_NUMBERS.size();++cnt){
			int problemNumber = ProblemSelection.PROBLEM_NUMBERS.get(cnt);

			//Get the problem
			Problem problem = ProblemSelection.getProblem(problemNumber);

			//Run each problem the specified number of times
			System.out.println(problemNumber + ". " + problem.getDescription());
			double totalTime = runProblem(problem, timesToRun);

			//Print the results
			System.out.println(getBenchmarkResults(problem, totalTime, timesToRun));
		}
	}
	//Asks how many times a problem is supposed to run and returns the value
	private static int getNumberOfTimesToRun(){
		int numOfTimesToRun = 1;
		System.out.print("How many times do you want to run this problem? ");
		numOfTimesToRun = input.nextInt();
		while(numOfTimesToRun < 1){
			System.out.println("That is an invalid number!");
			System.out.print("How many times do you want to run this problem? ");
			numOfTimesToRun = input.nextInt();
		}
		return numOfTimesToRun;
	}
	//Runs the problem the given number of times
	private static double runProblem(Problem problem, int timesToRun) throws InvalidResult{
		double totalTime = 0;
		System.out.print("Solving");
		for(int cnt = 0;cnt < timesToRun;++cnt){
			System.out.print('.');
			//Reset the data so you are actually counting the run time an additional time
			problem.reset();
			//Solve the problem
			problem.solve();
			//Get the time data
			totalTime += problem.getTimer().getNano();
		}
		return totalTime;
	}
	//Prints the benchmark results of a problem
	private static String getBenchmarkResults(Problem problem, double totalTime, int timesRun) throws InvalidResult{
		//Calculate the average run time of the problem
		totalTime /= timesRun;
		String timeResults = Stopwatch.getStr(totalTime);

		//Tally the results
		return "\n\n" + problem.getResult() + "\nIt took an average of " + timeResults + " to run this problem through " + timesRun + " iterations\n\n";
	}
}