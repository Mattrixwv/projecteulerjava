package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidParameterException;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem8 extends TestProblemBase{
	@InjectMocks
	private Problem8 problem;
	static{
		description = "Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?";
		result = String.format("The greatest product is 23514624000%nThe numbers are 5576689664895");
	}
	private String maxNums = "5576689664895";
	private long maxProduct = 23514624000L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getLargestNums(); });
		assertThrows(Unsolved.class, () -> { problem.getLargestProduct(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(maxNums, problem.getLargestNums());
		assertEquals(maxProduct, problem.getLargestProduct());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.maxNums = maxNums;
		problem.maxProduct = maxProduct;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals("", problem.maxNums);
		assertEquals(0L, problem.maxProduct);
	}

	@Test
	public void testSolve_readError() throws IOException{
		problem.reset();

		Problem8.number = null;
		RandomAccessFile file = new RandomAccessFile(Problem8.fileName, "rw");
		file.getChannel().lock();

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});

		file.close();
	}

	@Test
	public void testSolve_noFile(){
		problem.reset();

		Problem8.number = null;
		Problem8.fileName = "/a";

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});
	}
}
