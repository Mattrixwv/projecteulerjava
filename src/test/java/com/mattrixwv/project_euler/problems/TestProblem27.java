package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem27 extends TestProblemBase{
	@InjectMocks
	private Problem27 problem;
	static{
		description = "Find the product of the coefficients, |a| <= 999 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0";
		result = String.format("The greatest number of primes found is 70%nIt was found with A = -61, B = 971%nThe product of A and B is -59231");
	}
	private int topA = -61;
	private int topB = 971;
	private int topN = 70;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getTopA(); });
		assertThrows(Unsolved.class, () -> { problem.getTopB(); });
		assertThrows(Unsolved.class, () -> { problem.getTopN(); });
		assertThrows(Unsolved.class, () -> { problem.getProduct(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(topA, problem.getTopA());
		assertEquals(topB, problem.getTopB());
		assertEquals(topN, problem.getTopN());
		assertEquals(topA * topB, problem.getProduct());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.topA = topA;
		problem.topB = topB;
		problem.topN = topN;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.topA);
		assertEquals(0, problem.topB);
		assertEquals(0, problem.topN);
	}
}
