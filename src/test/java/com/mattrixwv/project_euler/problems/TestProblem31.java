package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem31 extends TestProblemBase{
	@InjectMocks
	private Problem31 problem;
	static{
		description = "How many different ways can 2 pounds be made using any number of coins?";
		result = "There are 73682 ways to make 2 pounds with the given denominations of coins";
	}
	private int permutations = 73682;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getPermutations(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(permutations, problem.getPermutations());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.permutations = permutations;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.permutations);
	}
}
