package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem10 extends TestProblemBase{
	@InjectMocks
	private Problem10 problem;
	static{
		description = "Find the sum of all the primes below 2000000.";
		result = "The sum of all the primes < 2000000 is 142913828922";
	}
	private long sum = 142913828922L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(sum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.sum);
	}
}
