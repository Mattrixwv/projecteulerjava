package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mattrixwv.Stopwatch;
import com.mattrixwv.project_euler.exceptions.Unsolved;


@ExtendWith(MockitoExtension.class)
public abstract class TestProblemBase{
	@Spy
	protected Stopwatch timer;
	protected static String description;
	protected static String result;

	public abstract void testDescription();
	public void testDescription(Problem problem){
		assertEquals(description, problem.getDescription());
	}

	public abstract void testSolve();
	public void testSolve(Problem problem){
		//Verify the results cannot be seen until the problem has been run
		assertFalse(problem.getSolved());
		assertThrows(Unsolved.class, () -> { problem.getResult(); });

		problem.solve();

		//Verify solved was set
		assertTrue(problem.getSolved());
		assertEquals(result, problem.getResult());
		//Verify timer function were called
		verify(timer, times(1)).start();
		verify(timer, times(1)).stop();

		//Verify the problem won't be run again
		problem.solve();

		//Verify solved is still set
		assertTrue(problem.getSolved());
		//Verify timer functions were not called again
		verify(timer, times(1)).start();
		verify(timer, times(1)).stop();
	}

	public abstract void testReset();
	public void testReset(Problem problem){
		assertTrue(problem.solved);

		problem.reset();

		//Verify solved was reset
		assertFalse(problem.getSolved());
		verify(timer, times(1)).reset();
		assertEquals(0, timer.getNano());
		verifyReset();

		//Verify the problem can be solved again
		testSolve();
	}

	public abstract void verifyReset();
}
