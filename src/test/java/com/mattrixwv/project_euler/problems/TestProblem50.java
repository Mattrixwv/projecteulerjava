package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem50 extends TestProblemBase{
	@InjectMocks
	private Problem50 problem;
	static{
		description = "Which prime, below 1000000, can be written as the sum of the most consecutive primes?";
		result = "The prime below 1000000 that can be written as the sum of the most consecutive primes is 997651";
	}
	private long prime = 997651L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getConsecutivePrimes(); });
		assertThrows(Unsolved.class, () -> { problem.getPrime(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(543, problem.getConsecutivePrimes().size());
		assertEquals(prime, problem.getPrime());
	}

	@Test
	@Override
	public void testReset(){
		problem.consecutivePrimes = new ArrayList<>(List.of(1L, 2L));
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.consecutivePrimes);
	}
}
