package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem20 extends TestProblemBase{
	@InjectMocks
	private Problem20 problem;
	static{
		description = "What is the sum of the digits of 100!?";
		result = String.format("100! = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000%nThe sum of the digits is: 648");
	}
	private String numString = "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000";
	private BigInteger num = new BigInteger(numString);
	private long sum = 648L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getNumber(); });
		assertThrows(Unsolved.class, () -> { problem.getNumberString(); });
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(numString, problem.getNumberString());
		assertEquals(num, problem.getNumber());
		assertEquals(sum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.num = num;
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(BigInteger.ONE, problem.num);
		assertEquals(0L, problem.sum);
	}
}
