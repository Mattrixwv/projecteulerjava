package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.security.InvalidParameterException;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem13 extends TestProblemBase{
	@InjectMocks
	private Problem13 problem;
	static{
		description = "Work out the first ten digits of the sum of the one-hundred 50-digit numbers";
		result = String.format("The sum of all 100 numbers is 5537376230390876637302048746832985971773659831892672%nThe first 10 digits of the sum of the numbers is 5537376230");
	}
	private BigInteger sum = new BigInteger("5537376230390876637302048746832985971773659831892672");


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getNumbers(); });
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(100, problem.getNumbers().size());
		assertEquals(sum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(BigInteger.ZERO, problem.sum);
	}

	@Test
	public void testSolve_readError() throws IOException{
		problem.reset();

		Problem13.nums = null;
		RandomAccessFile file = new RandomAccessFile(Problem13.fileName, "rw");
		file.getChannel().lock();

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});

		file.close();
	}

	@Test
	public void testSolve_noFile(){
		problem.reset();

		Problem13.nums = null;
		Problem13.fileName = "/a";

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});
	}
}
