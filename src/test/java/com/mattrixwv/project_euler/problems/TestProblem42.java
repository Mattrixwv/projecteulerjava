package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem42 extends TestProblemBase{
	@InjectMocks
	private Problem42 problem;
	static{
		description = "Triangular number t(n) - (n * (n + 1)) / 2. For A = 1, B = 2, ... find the number of trinagular words in the file";
		result = "The number of triangular numbers is 162";
	}


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getTriangularWords(); });
		assertThrows(Unsolved.class, () -> { problem.getNumberTriangularNumbers(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(162, problem.getTriangularWords().size());
		assertEquals(162, problem.getNumberTriangularNumbers());
	}

	@Test
	@Override
	public void testReset(){
		problem.triangleWords = new ArrayList<>(List.of("1", "2"));
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.triangleWords);
	}

	@Test
	public void testSolve_readError() throws IOException{
		problem.reset();

		Problem42.fileWords = new ArrayList<>();
		RandomAccessFile file = new RandomAccessFile(Problem42.fileName, "rw");
		file.getChannel().lock();

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});

		file.close();
	}

	@Test
	public void testSolve_noFile(){
		problem.reset();

		Problem42.fileWords = new ArrayList<>();
		Problem42.fileName = "/a";

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});
	}
}
