package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem36 extends TestProblemBase{
	@InjectMocks
	private Problem36 problem;
	static{
		description = "Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.";
		result = "The sum of all base 10 and base 2 palindromic numbers < 999999 is 872187";
	}
	private List<Integer> palindromes = List.of(1, 3, 5, 7, 9, 33, 99, 313, 585, 717, 7447, 9009, 15351, 32223, 39993, 53235, 53835, 73737, 585585);
	private int sum = 872187;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getPalindromes(); });
		assertThrows(Unsolved.class, () -> { problem.getSumOfPalindromes(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(palindromes, problem.getPalindromes());
		assertEquals(sum, problem.getSumOfPalindromes());
	}

	@Test
	@Override
	public void testReset(){
		problem.palindromes = new ArrayList<>(palindromes);
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.palindromes);
		assertEquals(0, problem.sum);
	}
}
