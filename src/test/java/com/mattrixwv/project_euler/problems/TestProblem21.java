package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem21 extends TestProblemBase{
	@InjectMocks
	private Problem21 problem;
	static{
		description = "Evaluate the sum of all the amicable numbers under 10000";
		result = String.format("All amicable numbers less than 10000 are%n220%n284%n1184%n1210%n2620%n2924%n5020%n5564%n6232%n6368%nThe sum of all of these amicable numbers is 31626");
	}
	private List<Integer> amicable = List.of(220, 284, 1184, 1210, 2620, 2924, 5020, 5564, 6232, 6368);
	private int sum = 31626;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getAmicable(); });
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(amicable, problem.getAmicable());
		assertEquals(sum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.amicable = new ArrayList<>(amicable);
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.amicable);
		ArrayList<Integer> divisorSum = new ArrayList<>(10000);
		while(divisorSum.size() < 10000){
			divisorSum.add(0);
		}
		assertEquals(divisorSum, problem.divisorSum);
	}
}
