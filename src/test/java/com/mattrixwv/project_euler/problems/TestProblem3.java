package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem3 extends TestProblemBase{
	@InjectMocks
	private Problem3 problem;
	static{
		description = "What is the largest prime factor of 600851475143?";
		result = "The largest factor of the number 600851475143 is 6857";
	}
	private static List<Long> factors = List.of(71L, 839L, 1471L, 6857L);
	private long largestFactor = 6857;



	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
		assertEquals(600851475143L, Problem3.getGoalNumber());
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getResult(); });
		assertThrows(Unsolved.class, () -> { problem.getFactors(); });
		assertThrows(Unsolved.class, () -> { problem.getLargestFactor(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(factors, problem.getFactors());
		assertEquals(largestFactor, problem.getLargestFactor());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.factors = new ArrayList<>(factors);
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.factors);
	}
}
