package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem33 extends TestProblemBase{
	@InjectMocks
	private Problem33 problem;
	static{
		description = "If the product of these four fractions is given in its lowest common terms, find the value of the denominator";
		result = "The denominator of the product is 100";
	}
	private List<Integer> numerators = List.of(16, 26, 19, 49);
	private List<Integer> denominators = List.of(64, 65, 95, 98);
	private int prod = 100;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getNumerators(); });
		assertThrows(Unsolved.class, () -> { problem.getDenominators(); });
		assertThrows(Unsolved.class, () -> { problem.getProdDenominator(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(numerators, problem.getNumerators());
		assertEquals(denominators, problem.getDenominators());
		assertEquals(prod, problem.getProdDenominator());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.numerators = new ArrayList<>(numerators);
		problem.denominators = new ArrayList<>(denominators);
		problem.prodDenominator = prod;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.numerators);
		assertEquals(new ArrayList<>(), problem.denominators);
		assertEquals(1, problem.prodDenominator);
	}
}
