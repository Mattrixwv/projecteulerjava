package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidParameterException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;
import com.mattrixwv.project_euler.problems.Problem18.Location;


public class TestProblem18 extends TestProblemBase{
	@InjectMocks
	private Problem18 problem;
	static{
		description = "Find the maximum total from top to bottom";
		result = "The value of the longest path is 1074";
	}
	private int total = 1074;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getTotal(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(total, problem.getTotal());
		assertEquals(String.format("75\n95 64\n17 47 82\n18 35 87 10\n20  4 82 47 65\n19  1 23 75  3 34\n88  2 77 73  7 63 67\n99 65  4 28  6 16 70 92\n41 41 26 56 83 40 80 70 33\n41 48 72 33 47 32 37 16 94 29\n53 71 44 65 25 43 91 52 97 51 14\n70 11 33 28 77 73 17 78 39 68 17 57\n91 71 52 38 17 14 91 43 58 50 27 29 48\n63 66  4 68 89 53 67 30 73 16 69 87 40 31\n 4 62 98 27 23  9 70 98 73 93 38 53 60  4 23"), problem.getPyramid());
		assertEquals("75->95->47->35->4->23->73->6->40->37->91->78->58->73->93", problem.getTrail());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.actualTotal = total;
		problem.foundPoints = new ArrayList<>();
		problem.foundPoints.add(new Location(0, 0, 0, true));
		problem.possiblePoints = new ArrayList<>();
		problem.possiblePoints.add(new Location(0, 0, 0, true));
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals("files/Problem18Pyramid.txt", problem.fileName);
		assertEquals(new ArrayList<>(), problem.foundPoints);
		assertEquals(new ArrayList<>(), problem.possiblePoints);
		assertEquals(0, problem.actualTotal);
	}

	@Test
	public void testSolve_readError() throws IOException{
		problem.reset();

		Problem18.list = null;
		RandomAccessFile file = new RandomAccessFile(problem.fileName, "rw");
		file.getChannel().lock();

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});

		file.close();
	}

	@Test
	public void testSolve_noFile(){
		problem.reset();

		Problem18.list = null;
		problem.fileName = "/a";

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});
	}

	@Test
	public void testLocation(){
		int x = 1;
		int y = 2;
		int t = 3;
		boolean r = true;
		Location location = new Location(x, y, t, r);

		assertEquals(x, location.getXLocation());
		assertEquals(y, location.getYLocation());
		assertEquals(t, location.getTotal());
		assertTrue(location.getFromRight());

		x = 4;
		y = 5;
		t = 6;
		r = false;
		location.setXLocation(x);
		location.setYLocation(y);
		location.setTotal(t);
		location.setFromRight(r);

		assertEquals(x, location.getXLocation());
		assertEquals(y, location.getYLocation());
		assertEquals(t, location.getTotal());
		assertFalse(location.getFromRight());
	}
}
