package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidParameterException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem22 extends TestProblemBase{
	@InjectMocks
	private Problem22 problem;
	static{
		description = "What is the total of all the name scores in this file?";
		result = "The answer to the question is 871198282";
	}
	private long sum = 871198282L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getNameScoreSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(5163, Problem22.getNames().size());
		assertEquals(sum, problem.getNameScoreSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.sums = new ArrayList<>();
		problem.sums.add(1L);
		problem.prod = new ArrayList<>();
		problem.prod.add(2L);
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.sums);
		assertEquals(new ArrayList<>(), problem.prod);
		assertEquals(0L, problem.sum);
	}

	@Test
	public void testSolve_readError() throws IOException{
		problem.reset();

		Problem22.names = new ArrayList<>();
		RandomAccessFile file = new RandomAccessFile(Problem22.fileName, "rw");
		file.getChannel().lock();

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});

		file.close();
	}

	@Test
	public void testSolve_noFile(){
		problem.reset();

		Problem22.names = new ArrayList<>();
		Problem22.fileName = "/a";

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});
	}
}
