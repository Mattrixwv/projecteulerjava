package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.NumberAlgorithms;
import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem12 extends TestProblemBase{
	@InjectMocks
	private Problem12 problem;
	static{
		description = "What is the value of the first triangle number to have over 500 divisors?";
		result = "The triangular number 76576500 is the sum of all numbers >= 12375 and has 576 divisors";
	}
	private long sum = 76576500;
	private long counter = 12375;
	private List<Long> divisors = NumberAlgorithms.getDivisors(sum);


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getTriangularNumber(); });
		assertThrows(Unsolved.class, () -> { problem.getLastNumberAdded(); });
		assertThrows(Unsolved.class, () -> { problem.getDivisorsOfTriangularNumber(); });
		assertThrows(Unsolved.class, () -> { problem.getNumberOfDivisors(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(sum, problem.getTriangularNumber());
		assertEquals(counter, problem.getLastNumberAdded());
		assertEquals(divisors, problem.getDivisorsOfTriangularNumber());
		assertEquals(divisors.size(), problem.getNumberOfDivisors());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.sum = sum;
		problem.counter = counter;
		problem.divisors = new ArrayList<>(divisors);
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(1, problem.sum);
		assertEquals(2, problem.counter);
		assertEquals(new ArrayList<>(), problem.divisors);
	}
}
