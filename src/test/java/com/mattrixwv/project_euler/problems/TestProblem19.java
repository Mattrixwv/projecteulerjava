package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem19 extends TestProblemBase{
	@InjectMocks
	private Problem19 problem;
	static{
		description = "How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?";
		result = "There are 171 Sundays that landed on the first of the months from 1901 to 2000";
	}
	private long totalSundays = 171L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getTotalSundays(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(totalSundays, problem.getTotalSundays());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.totalSundays = totalSundays;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.totalSundays);
	}
}
