package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mattrixwv.project_euler.exceptions.Unsolved;


@ExtendWith(MockitoExtension.class)
public class TestProblem{
	private Problem problem;


	@BeforeEach
	public void setup(){
		problem = new Problem1();
	}

	@Test
	public void testConstructor(){
		assertNotNull(problem);
		assertNotNull(problem.getDescription());
		assertFalse(problem.getSolved());
	}

	@Test
	public void testGetDescription(){
		assertNotNull(problem.getDescription());
	}

	@Test
	public void testGetTime(){
		assertThrows(Unsolved.class, () -> {
			problem.getTime();
		});

		problem.solved = true;

		assertNotNull(problem.getTime());
	}

	@Test
	public void testGetTimer(){
		assertThrows(Unsolved.class, () -> {
			problem.getTimer();
		});

		problem.solved = true;
		assertNotNull(problem.getTimer());
	}

	@Test
	public void testSolvedCheck(){
		assertThrows(Unsolved.class, () -> {
			problem.solvedCheck("");
		});

		problem.solved = true;
		problem.solvedCheck("");
	}

	@Test
	public void testGetSolved(){
		assertFalse(problem.getSolved());

		problem.solved = true;
		assertTrue(problem.getSolved());
	}

	@Test
	public void testReset(){
		problem.solved = true;

		problem.reset();

		assertFalse(problem.getSolved());
		problem.solved = true;
		assertEquals(0.0D, problem.getTimer().getNano());
	}
}
