package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem11 extends TestProblemBase{
	@InjectMocks
	private Problem11 problem;
	static{
		description = "What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?";
		result = String.format("The greatest product of 4 numbers in a line is 70600674%nThe numbers are [89, 94, 97, 87]");
	}
	private List<Integer> greatestProduct = List.of(89, 94, 97, 87);
	private int product = 70600674;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getNumbers(); });
		assertThrows(Unsolved.class, () -> { problem.getProduct(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(greatestProduct, problem.getNumbers());
		assertEquals(product, problem.getProduct());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.greatestProduct = new ArrayList<>(greatestProduct);
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.greatestProduct);
	}

	@Test
	public void testSolve_readError() throws IOException{
		problem.reset();

		Problem11.grid = null;
		RandomAccessFile file = new RandomAccessFile(Problem11.fileName, "rw");
		file.getChannel().lock();

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});

		file.close();
	}

	@Test
	public void testSolve_noFile(){
		problem.reset();

		Problem11.grid = null;
		Problem11.fileName = "/a";

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});
	}
}
