package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem14 extends TestProblemBase{
	@InjectMocks
	private Problem14 problem;
	static{
		description = "Which starting number, under 1000000, produces the longest chain using the iterative sequence?";
		result = "The number 837799 produced a chain of 525 steps";
	}
	private long maxLength = 525L;
	private long maxNum = 837799L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getLength(); });
		assertThrows(Unsolved.class, () -> { problem.getStartingNumber(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(maxLength, problem.getLength());
		assertEquals(maxNum, problem.getStartingNumber());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.maxLength = maxLength;
		problem.maxNum = maxNum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0L, problem.maxLength);
		assertEquals(0L, problem.maxNum);
	}
}
