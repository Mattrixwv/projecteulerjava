package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem38 extends TestProblemBase{
	@InjectMocks
	private Problem38 problem;
	static{
		description = "What is the largest 1-9 pandigital number that can be formed as the concatenated product of an integer with 1, 2, ... n where n > 1";
		result = "The largest appended product pandigital is 932718654";
	}
	private long largestNum = 9327L;
	private long pandigital = 932718654L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getLargestNum(); });
		assertThrows(Unsolved.class, () -> { problem.getPandigital(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(largestNum, problem.getLargestNum());
		assertEquals(pandigital, problem.getPandigital());
	}

	@Test
	@Override
	public void testReset(){
		problem.largestNum = largestNum;
		problem.pandigital = pandigital;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.largestNum);
		assertEquals(0, problem.pandigital);
	}
}
