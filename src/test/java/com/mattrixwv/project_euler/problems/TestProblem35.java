package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem35 extends TestProblemBase{
	@InjectMocks
	private Problem35Override problem;
	static{
		description = "How many circular primes are there below 10000?";
		result = "The number of all circular prime numbers under 9999 is 33";
	}


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getPrimes(); });
		assertThrows(Unsolved.class, () -> { problem.getCircularPrimes(); });
		assertThrows(Unsolved.class, () -> { problem.getNumCircularPrimes(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(1229, problem.getPrimes().size());
		assertEquals(33, problem.getCircularPrimes().size());
		assertEquals(33, problem.getNumCircularPrimes());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.primes = new ArrayList<>();
		problem.primes.add(1);
		problem.circularPrimes = new ArrayList<>();
		problem.circularPrimes.add(2);
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.primes);
		assertEquals(new ArrayList<>(), problem.circularPrimes);
	}


	public static class Problem35Override extends Problem35{
		static{
			maxNum = 9999;
		}
	}
}
