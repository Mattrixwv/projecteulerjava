package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;
import com.mattrixwv.project_euler.problems.Problem32.ProductSet;


public class TestProblem32 extends TestProblemBase{
	@InjectMocks
	private Problem32 problem;
	static{
		description = "Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.";
		result = String.format("There are 7 unique 1-9 pandigitals%nThe sum of the products of these pandigitals is 45228");
	}
	private long sum = 45228L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getSumOfPandigitals(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(sum, problem.getSumOfPandigitals());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.listOfProducts = new ArrayList<>();
		problem.listOfProducts.add(problem.new ProductSet(5, 6));
		problem.sumOfPandigitals = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.listOfProducts);
		assertEquals(0, problem.sumOfPandigitals);
	}

	@Test
	public void testProductSet(){
		int multiplicand = 2;
		int multiplier = 3;
		ProductSet ps = new Problem32().new ProductSet(multiplicand, multiplier);

		assertEquals(multiplicand, ps.getMultiplicand());
		assertEquals(multiplier, ps.getMultiplier());
		assertEquals(multiplicand * multiplier, ps.getProduct());
		assertEquals(ps, ps);
		assertNotEquals(ps, 0);
		assertEquals(ps, new Problem32().new ProductSet(multiplicand, multiplier));
		assertEquals(((2 * multiplicand) + (3 * multiplier)), ps.hashCode());
		assertEquals("236", ps.toString());
	}
}
