package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem5 extends TestProblemBase{
	@InjectMocks
	private Problem5 problem;
	static{
		description = "What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?";
		result = "The smallest positive number evenly divisible by all numbers 1-20 is 232792560";
	}
	private int smallestNum = 232792560;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getNumber(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(smallestNum, problem.getNumber());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.smallestNum = smallestNum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.smallestNum);
	}
}
