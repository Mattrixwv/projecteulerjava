package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem28 extends TestProblemBase{
	@InjectMocks
	private Problem28 problem;
	static{
		description = "What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction?";
		result = "The sum of the diagonals in the given grid is 669171001";
	}
	private int sum = 669171001;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getGrid(); });
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertNotNull(problem.getGrid());
		assertEquals(sum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.grid = new ArrayList<>();
		problem.grid.add(new ArrayList<>());
		problem.grid.get(0).add(1);
		problem.sumOfDiagonals = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		ArrayList<ArrayList<Integer>> grid = new ArrayList<>();
		grid.add(new ArrayList<>());
		grid.get(0).add(1);
		assertEquals(grid, problem.grid);
		assertEquals(0, problem.sumOfDiagonals);
	}
}
