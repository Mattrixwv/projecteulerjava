package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem2 extends TestProblemBase{
	@InjectMocks
	private Problem2 problem;
	static{
		description = "What is the sum of the even Fibonacci numbers less than 4000000?";
		result = "The sum of all even fibonacci numbers <= 3999999 is 4613732";
	}
	private int fullSum = 4613732;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getResult(); });
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(fullSum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.fullSum = fullSum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.fullSum);
	}
}
