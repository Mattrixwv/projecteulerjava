package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem34 extends TestProblemBase{
	@InjectMocks
	private Problem34 problem;
	static{
		description = "Find the sum of all numbers which are equal to the sum of the factorial of their digits";
		result = "The sum of all numbers that are the sum of their digit's factorials is 40730";
	}
	private List<Integer> factorials = List.of(1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880);
	private int sum = 40730;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getFactorials(); });
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(factorials, problem.getFactorials());
		assertEquals(sum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.factorials = new ArrayList<>(factorials);
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(List.of(0, 0, 0, 0, 0, 0, 0, 0, 0, 0), problem.factorials);
		assertEquals(0, problem.sum);
	}
}
