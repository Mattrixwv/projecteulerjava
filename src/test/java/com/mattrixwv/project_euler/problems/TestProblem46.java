package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem46 extends TestProblemBase{
	@InjectMocks
	private Problem46 problem;
	static{
		description = "What is the smallest odd composite number that connot be written as the sum of a prime and twice a square?";
		result = "The smallest odd composite that cannot be written as the sum of a prime and twice a square is 5777";
	}
	private long compositeNum = 5777L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getCompositeNum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(compositeNum, problem.getCompositeNum());
	}

	@Test
	@Override
	public void testReset(){
		problem.num = compositeNum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0L, problem.num);
	}
}
