package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


@Execution(ExecutionMode.CONCURRENT)
public class TestProblem6 extends TestProblemBase{
	@InjectMocks
	private Problem6 problem;
	static{
		description = "Find the difference between the sum of the squares and the square of the sum of the numbers 1-100.";
		result = "The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is 25164150";
	}
	private long sumOfSquares = 338350L;
	private long squareOfSum = 25502500L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getSumOfSquares(); });
		assertThrows(Unsolved.class, () -> { problem.getSquareOfSum(); });
		assertThrows(Unsolved.class, () -> { problem.getDifference(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(sumOfSquares, problem.getSumOfSquares());
		assertEquals(squareOfSum, problem.getSquareOfSum());
		assertEquals(squareOfSum - sumOfSquares, problem.getDifference());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.sumOfSquares = sumOfSquares;
		problem.squareOfSum = squareOfSum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0L, problem.sumOfSquares);
		assertEquals(0L, problem.squareOfSum);
	}
}
