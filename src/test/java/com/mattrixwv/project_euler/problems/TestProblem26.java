package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem26 extends TestProblemBase{
	@InjectMocks
	private Problem26 problem;
	static{
		description = "Find the value of d <= 999 for which 1/d contains the longest recurring cycle in its decimal fraction part.";
		result = String.format("The longest cycle is 982 digits long%nIt started with the number 983");
	}
	private int longestCycle = 982;
	private int longestNumber = 983;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getLongestCycle(); });
		assertThrows(Unsolved.class, () -> { problem.getLongestNumber(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(longestCycle, problem.getLongestCycle());
		assertEquals(longestNumber, problem.getLongestNumber());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.longestCycle = longestCycle;
		problem.longestNumber = longestNumber;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.longestCycle);
		assertEquals(1, problem.longestNumber);
	}
}
