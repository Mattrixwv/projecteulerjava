package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem43 extends TestProblemBase{
	@InjectMocks
	private Problem43 problem;
	static{
		description = "Find the sum of all 0-9 pandigital numbers with this property";
		result = "The sum of all pandigitals with the property is 1113342912";
	}
	private List<Long> subPrimePandigitals = List.of(140635728L, 146035728L, 410635728L, 416035728L);
	private long sumSubPrimePandigitals = 1113342912L;


	@BeforeEach
	public void setup(){
		Problem43.nums = "012345678";
	}


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getSubPrimePandigitals(); });
		assertThrows(Unsolved.class, () -> { problem.getSumSubPrimePandigitals(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(subPrimePandigitals, problem.getSubPrimePandigitals());
		assertEquals(sumSubPrimePandigitals, problem.getSumSubPrimePandigitals());
	}

	@Test
	@Override
	public void testReset(){
		problem.subPrimePandigitals = new ArrayList<>(subPrimePandigitals);
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.subPrimePandigitals);
	}
}
