package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.generators.PentagonalNumberGenerator;
import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem44 extends TestProblemBase{
	@InjectMocks
	private Problem44 problem;
	static{
		description = "Find 2 pentagonal numbers whos sum and difference are also pentagonal numbers";
		result = "The difference of the pentagonal numbers is 5482660";
	}
	private long pentagonalNumber1 = 1560090L;
	private long pentagonalNumber2 = 7042750L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getPentagonalList(); });
		assertThrows(Unsolved.class, () -> { problem.getPentagonalNumber1(); });
		assertThrows(Unsolved.class, () -> { problem.getPentagonalNumber2(); });
		assertThrows(Unsolved.class, () -> { problem.getDifference(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(2167, problem.getPentagonalList().size());
		assertEquals(pentagonalNumber1, problem.getPentagonalNumber1());
		assertEquals(pentagonalNumber2, problem.getPentagonalNumber2());
		assertEquals(pentagonalNumber2 - pentagonalNumber1, problem.getDifference());
	}

	@Test
	@Override
	public void testReset(){
		problem.pentagonalList = new ArrayList<>(List.of(1L, 2L, 3L));
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new PentagonalNumberGenerator().next(), problem.generator.next());
		problem.generator = new PentagonalNumberGenerator();
		assertEquals(new ArrayList<>(), problem.pentagonalList);
		assertEquals(0, problem.pentagonalNumber1);
		assertEquals(0, problem.pentagonalNumber2);
	}
}
