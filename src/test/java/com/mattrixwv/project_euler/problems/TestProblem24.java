package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem24 extends TestProblemBase{
	@InjectMocks
	private Problem24Override problem;
	static{
		description = "What is the 100th lexicographic permutation of the digits 01234?";
		result = "The 100th permutation is 40231";
	}


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getPermutationsList(); });
		assertThrows(Unsolved.class, () -> { problem.getPermutation(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(120, problem.getPermutationsList().size());
		assertEquals("40231", problem.getPermutation());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.permutations = new ArrayList<>();
		problem.permutations.add("1");
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.permutations);
	}


	public static class Problem24Override extends Problem24{
		static{
			neededPerm = 100;
			nums = "01234";
		}
	}
}
