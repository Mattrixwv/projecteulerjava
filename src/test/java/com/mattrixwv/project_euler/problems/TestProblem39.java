package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.Triple;
import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem39 extends TestProblemBase{
	@InjectMocks
	private Problem39 problem;
	static{
		description = "If p is the perimeter of a right triangle for which value of p <= 100 is the number of solutions for the sides {a, b, c} maximized";
		result = "The perimeter with the largest number of solutions is 90";
	}
	private List<Triple<Long, Long, Long>> longestSolutions = List.of(new Triple<>(9L, 40L, 41L), new Triple<>(15L, 36L, 39L));


	@BeforeEach
	public void setup(){
		Problem39.maxPerimeter = 100;
	}


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getLongestSolutions(); });
		assertThrows(Unsolved.class, () -> { problem.getLongestPerimeter(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(longestSolutions, problem.getLongestSolutions());
		assertEquals(90L, problem.getLongestPerimeter());
	}

	@Test
	@Override
	public void testReset(){
		problem.longestSolutions = new ArrayList<>(longestSolutions);
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.longestSolutions);
	}
}
