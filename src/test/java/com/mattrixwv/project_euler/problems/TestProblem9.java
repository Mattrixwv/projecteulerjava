package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem9 extends TestProblemBase{
	@InjectMocks
	private Problem9 problem;
	static{
		description = "There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.";
		result = String.format("The Pythagorean triplet is 200 + 375 + 425%nThe numbers' product is 31875000");
	}
	private int a = 200;
	private int b = 375;
	private int c = 425;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getSideA(); });
		assertThrows(Unsolved.class, () -> { problem.getSideB(); });
		assertThrows(Unsolved.class, () -> { problem.getSideC(); });
		assertThrows(Unsolved.class, () -> { problem.getProduct(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(a, problem.getSideA());
		assertEquals(b, problem.getSideB());
		assertEquals(c, problem.getSideC());
		assertEquals(a * b * c, problem.getProduct());
	}

	@Test
	@Override
	public void testReset(){
		problem.a = a;
		problem.b = b;
		problem.c = c;
		problem.found = true;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(1, problem.a);
		assertEquals(0, problem.b);
		assertEquals(0, problem.c);
		assertEquals(false, problem.found);
	}

	@Test
	public void testSolve_notFound(){
		problem.reset();
		problem.goalSum = 1;

		assertThrows(Unsolved.class, () -> {
			problem.solve();
		});
	}
}
