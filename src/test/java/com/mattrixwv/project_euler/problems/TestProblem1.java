package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem1 extends TestProblemBase{
	@InjectMocks
	private Problem1 problem = new Problem1();
	static{
		description = "What is the sum of all the multiples of 3 or 5 that are less than 1000?";
		result = "The sum of all numbers < 1000 is 233168";
	}
	private int fullSum = 233168;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getResult(); });
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(fullSum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.fullSum = fullSum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.fullSum);
	}
}
