package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidParameterException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;
import com.mattrixwv.project_euler.problems.Problem18.Location;


public class TestProblem67 extends TestProblemBase{
	@InjectMocks
	private Problem67 problem;
	static{
		description = "Find the maximum total from top to bottom";
		result = "The value of the longest path is 7273";
	}
	private int total = 7273;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getTotal(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(total, problem.getTotal());
	}

	@Test
	@Override
	public void testReset(){
		problem.actualTotal = total;
		problem.foundPoints = new ArrayList<>();
		problem.foundPoints.add(new Location(0, 0, 0, true));
		problem.possiblePoints = new ArrayList<>();
		problem.possiblePoints.add(new Location(0, 0, 0, true));
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals("files/Problem67Pyramid.txt", problem.fileName);
		assertEquals(new ArrayList<>(), problem.foundPoints);
		assertEquals(new ArrayList<>(), problem.possiblePoints);
		assertEquals(0, problem.actualTotal);
	}

	@Test
	public void testSolve_readError() throws IOException{
		problem.reset();

		Problem67.list = null;
		RandomAccessFile file = new RandomAccessFile(problem.fileName, "rw");
		file.getChannel().lock();

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});

		file.close();
	}

	@Test
	public void testSolve_noFile(){
		problem.reset();

		Problem67.list = null;
		problem.fileName = "/a";

		assertThrows(InvalidParameterException.class, () -> {
			problem.solve();
		});
	}
}
