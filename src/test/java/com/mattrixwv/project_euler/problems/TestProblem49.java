package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem49 extends TestProblemBase{
	@InjectMocks
	private Problem49 problem;
	static{
		description = "What is the 12-digit number formed by concatenating the three terms in the sequence?";
		result = "The 12-digit number formed by concatenation the three terms in the sequence is 296962999629";
	}
	private String concatNums = "296962999629";


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getConcatNums(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(concatNums, problem.getConcatNums());
	}

	@Test
	@Override
	public void testReset(){
		problem.concatenationOfNumbers = concatNums;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals("", problem.concatenationOfNumbers);
	}
}
