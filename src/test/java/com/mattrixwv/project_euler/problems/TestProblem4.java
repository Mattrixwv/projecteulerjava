package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem4 extends TestProblemBase{
	@InjectMocks
	private Problem4 problem;
	static{
		description = "Find the largest palindrome made from the product of two 3-digit numbers";
		result = "The largest palindrome is 906609";
	}


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getPalindromes(); });
		assertThrows(Unsolved.class, () -> { problem.getLargestPalindrome(); });

		super.testSolve(problem);

		//verify result
		assertEquals(1239, problem.getPalindromes().size());
		assertEquals(906609, problem.getLargestPalindrome());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		Problem4 p2 = new Problem4();
		p2.solve();
		List<Integer> palindromes = p2.getPalindromes();
		problem.palindromes = palindromes;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.palindromes);
	}
}
