package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem15 extends TestProblemBase{
	@InjectMocks
	private Problem15Override problem;
	static{
		description = "How many routes from the top left corner to the bottom right corner are there through a 5x5 grid if you can only move right and down?";
		result = "The number of routes is 252";
	}
	private long numOfRoutes = 252;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getNumberOfRoutes(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(numOfRoutes, problem.getNumberOfRoutes());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.numOfRoutes = numOfRoutes;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0L, problem.numOfRoutes);
	}

	public static class Problem15Override extends Problem15{
		static{
			gridWidth = 5;
			gridLength = 5;
		}
	}
}
