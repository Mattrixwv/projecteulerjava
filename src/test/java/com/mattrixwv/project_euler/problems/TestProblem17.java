package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem17 extends TestProblemBase{
	@InjectMocks
	private Problem17 problem;
	static{
		description = "If all the numbers from 1 to 1000 inclusive were written out in words, how many letters would be used?";
		result = "The sum of all the letters in all the numbers 1-1000 is 21124";
	}
	private long letterCount = 21124L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getLetterCount(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(letterCount, problem.getLetterCount());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.letterCount = letterCount;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.letterCount);
	}

	@Test
	public void testMakeWordFromNum_negative(){
		String numberString = problem.makeWordFromNum(-1);

		assertEquals("negative one", numberString);
	}

	@Test
	public void testMakeWordFromNum_zero(){
		String numberString = problem.makeWordFromNum(0);

		assertEquals("zero", numberString);
	}
}
