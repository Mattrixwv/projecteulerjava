package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem41 extends TestProblemBase{
	@InjectMocks
	private Problem41 problem;
	static{
		description = "What is the largest n-digit pandigital prime?";
		result = "The largest n-digit pandigital prime is 7652413";
	}
	private long largestPrimePandigital = 7652413L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getLargestPrimePandigital(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(largestPrimePandigital, problem.getLargestPrimePandigital());
	}

	@Test
	@Override
	public void testReset(){
		problem.largestPrimePandigital = largestPrimePandigital;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.largestPrimePandigital);
	}
}
