package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem16 extends TestProblemBase{
	@InjectMocks
	private Problem16 problem;
	static{
		description = "What is the sum of the digits of the number 2^1000?";
		result = String.format("2^1000 = 10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376%nThe sum of the elements is 1366");
	}
	private BigInteger num = new BigInteger("10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376");
	private int sum = 1366;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> {problem.getNumber(); });
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(num, problem.getNumber());
		assertEquals(sum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.num = num;
		problem.sumOfElements = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(BigInteger.ZERO, problem.num);
		assertEquals(0, problem.sumOfElements);
	}
}
