package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem40 extends TestProblemBase{
	@InjectMocks
	private Problem40 problem;
	static{
		description = "An irrational decimal fraction is created by concatenating the positive integers. Find the value of the following expression: d1 * d10 * d100 * d1000 * d10000 * d100000 * d1000000";
		result = "The product is 210";
	}
	private long product = 210L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getIrrationalDecimal(); });
		assertThrows(Unsolved.class, () -> { problem.getProduct(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(1000007, problem.getIrrationalDecimal().length());
		assertEquals(product, problem.getProduct());
	}

	@Test
	@Override
	public void testReset(){
		problem.irrationalDecimal = new StringBuilder("Some random string");
		problem.product = product;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals("", problem.irrationalDecimal.toString());
		assertEquals(0, problem.product);
	}
}
