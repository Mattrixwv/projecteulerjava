package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.NumberAlgorithms;
import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem7 extends TestProblemBase{
	@InjectMocks
	private Problem7 problem;
	static{
		description = "What is the 10001th prime number?";
		result = "The 10001th prime number is 104743";
	}


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getPrime(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(104743L, problem.getPrime());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.primes = NumberAlgorithms.getNumPrimes(10001L);
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.primes);
	}
}
