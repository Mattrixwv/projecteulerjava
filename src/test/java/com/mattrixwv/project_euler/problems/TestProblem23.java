package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem23 extends TestProblemBase{
	@InjectMocks
	private Problem23 problem;
	static{
		description = "Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers";
		result = "The answer is 2035227";
	}
	private long sum = 2035227L;


	@BeforeEach
	public void setup(){
		Problem23.maxNum = 5000;
	}


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getSum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(sum, problem.getSum());
	}

	@Test
	@Override
	public void testReset(){
		//Setup
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0L, problem.sum);
		ArrayList<Integer> divisorSums = new ArrayList<>(5000);
		while(divisorSums.size() <= 5000){
			divisorSums.add(0);
		}
		assertEquals(divisorSums, problem.divisorSums);
	}
}
