package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem37 extends TestProblemBase{
	@InjectMocks
	private Problem37 problem;
	static{
		description = "Find the sum of the only eleven primes that are both truncatable from left to right and right to left (2, 3, 5, and 7 are not counted).";
		result = "The sum of all left and right truncatable primes is 748317";
	}
	private List<Long> truncPrimes = List.of(23L, 37L, 53L, 73L, 313L, 317L, 373L, 797L, 3137L, 3797L, 739397L);
	private long sum = 748317L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getTruncatablePrimes(); });
		assertThrows(Unsolved.class, () -> { problem.getSumOfPrimes(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(truncPrimes, problem.getTruncatablePrimes());
		assertEquals(sum, problem.getSumOfPrimes());
	}

	@Test
	@Override
	public void testReset(){
		problem.truncPrimes = new ArrayList<>(truncPrimes);
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.truncPrimes);
		assertEquals(0L, problem.sum);
	}
}
