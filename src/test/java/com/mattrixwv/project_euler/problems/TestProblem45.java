package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.generators.HexagonalNumberGenerator;
import com.mattrixwv.generators.PentagonalNumberGenerator;
import com.mattrixwv.generators.TriangularNumberGenerator;
import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem45 extends TestProblemBase{
	@InjectMocks
	private Problem45 problem;
	static{
		description = "Find the next triangle number after 40755 that is also pentagonal and hexagonal";
		result = "The next triangular/pentagonal/hexagonal number is 1533776805";
	}
	private long num = 1533776805L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getNum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(num, problem.getNum());
	}

	@Test
	@Override
	public void testReset(){
		problem.num = num;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.num);
		assertEquals(new TriangularNumberGenerator().next(), problem.triGen.next());
		problem.triGen = new TriangularNumberGenerator();
		assertEquals(new PentagonalNumberGenerator().next(), problem.penGen.next());
		problem.penGen = new PentagonalNumberGenerator();
		assertEquals(new HexagonalNumberGenerator().next(), problem.hexGen.next());
		problem.hexGen = new HexagonalNumberGenerator();
	}
}
