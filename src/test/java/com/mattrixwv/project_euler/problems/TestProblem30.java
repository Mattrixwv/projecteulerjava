package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem30 extends TestProblemBase{
	@InjectMocks
	private Problem30 problem;
	static{
		description = "Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.";
		result = String.format("The sum of all the numbers that can be written as the sum of the fifth powers of their digits is 443839");
	}
	private List<Long> sumOfFifthNumbers = List.of(4150L, 4151L, 54748L, 92727L, 93084L, 194979L);
	private long sum = 443839L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getListOfSumOfFifths(); });
		assertThrows(Unsolved.class, () -> { problem.getSumOfList(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(sumOfFifthNumbers, problem.getListOfSumOfFifths());
		assertEquals(sum, problem.getSumOfList());
		assertEquals(1000000L, Problem30.getTopNum());
	}

	@Test
	@Override
	public void testReset(){
		problem.sumOfFifthNumbers = new ArrayList<>(sumOfFifthNumbers);
		problem.sum = sum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(new ArrayList<>(), problem.sumOfFifthNumbers);
		assertEquals(0L, problem.sum);
	}
}
