package com.mattrixwv.project_euler.problems;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import com.mattrixwv.project_euler.exceptions.Unsolved;


public class TestProblem47 extends TestProblemBase{
	@InjectMocks
	private Problem47 problem;
	static{
		description = "What is the first of four consecutive integers to have four distinct prime factors each?";
		result = "The first number is 134043";
	}
	private long firstNum = 134043L;


	@Test
	@Override
	public void testDescription(){
		super.testDescription(problem);
	}

	@Test
	@Override
	public void testSolve(){
		assertThrows(Unsolved.class, () -> { problem.getFirstNum(); });

		super.testSolve(problem);

		//Verify result
		assertEquals(firstNum, problem.getFirstNum());
	}

	@Test
	@Override
	public void testReset(){
		problem.num = firstNum;
		problem.solved = true;

		super.testReset(problem);
	}

	@Override
	public void verifyReset(){
		assertEquals(0, problem.num);
	}
}
