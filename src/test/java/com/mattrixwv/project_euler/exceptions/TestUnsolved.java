package com.mattrixwv.project_euler.exceptions;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;


public class TestUnsolved{
	@Test
	public void testUnsolved(){
		Unsolved unsolved = new Unsolved();
		assertNull(unsolved.getMessage());

		String testMessage = "Test message";
		unsolved = new Unsolved(testMessage);
		assertEquals(testMessage, unsolved.getMessage());
	}
}
